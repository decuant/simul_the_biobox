// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// DlgPlotSettings.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "PlotView.h"						// we will attach a CPlotView obj

// ----------------------------------------------------------------------------
// struct stRevertSettings
// ----------------------------------------------------------------------------

struct stRevertSettings
{
	CPoint		m_ptViewport;
	CPoint		m_ptRefPos;
	float		m_ScaleX;
	float		m_ScaleY;

	stRevertSettings()
	{
		m_ScaleX = m_ScaleY = 0.0f;
	}
};

// ----------------------------------------------------------------------------
// CDlgPlotSettings
// ----------------------------------------------------------------------------

class CDlgPlotSettings : public CDialogEx
{
protected:

	DECLARE_DYNAMIC(CDlgPlotSettings)

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_PLOTSETTINGS };
#endif

	// members
	//
	CBrush				m_DlgBrushBack;			// brush for the background

	stRevertSettings	m_Settings;				// original settings read from the plotter
	bool				m_bInitDialog;

	CEdit				m_EdRefPosX;			// reference line
	CEdit				m_EdRefPosY;
	CSpinButtonCtrl		m_SpinX;
	CSpinButtonCtrl		m_SpinY;

	CEdit				m_EdScaleX;				// scaling
	CEdit				m_EdScaleY;
	CSpinButtonCtrl		m_SpinScaleX;
	CSpinButtonCtrl		m_SpinScaleY;

	CEdit				m_EdOffsetX;			// viewport
	CEdit				m_EdOffsetY;
	CSpinButtonCtrl		m_SpinOffsetX;
	CSpinButtonCtrl		m_SpinOffsetY;

	CStatic				m_picViewPort;			// viewport controller

	CPlotView			* m_Plotter;			// the obj we want to change settings

protected:

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog(void);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

	afx_msg void OnEnUpdateEdrefX();
	afx_msg void OnEnUpdateEdrefY();
	afx_msg void OnEnUpdateEdscaleX();
	afx_msg void OnEnUpdateEdscaleY();
	afx_msg void OnDeltaposSpinScalex(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnDeltaposSpinScaley(NMHDR *pNMHDR, LRESULT *pResult);

	afx_msg void OnBnClickedOriginalRef();
	afx_msg void OnBnClickedOriginalScale();
	afx_msg void OnBnClickedOriginalZero();

	afx_msg void OnEnUpdateEdzeroX();
	afx_msg void OnEnUpdateEdzeroY();

	DECLARE_MESSAGE_MAP()

	int		GetIntegerFromEdit(const CEdit & inEditCtrl);
	float	GetFloatFromEdit(const CEdit & inEditCtrl);

	void	ReadAndSetRefPoint(void);
	void	ReadAndSetScale(void);
	void	ReadAndSetOffset(void);

public:

	CDlgPlotSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgPlotSettings();

	// set the object we are dealing with
	//
	void AttachToPlotter(CPlotView * inPlotter);



};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
