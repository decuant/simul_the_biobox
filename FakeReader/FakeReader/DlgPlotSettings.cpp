// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// DlgPlotSettings.cpp : implementation file
//

#include "stdafx.h"
#include "FakeReader.h"
#include "DlgPlotSettings.h"
#include "afxdialogex.h"
#include "C_Colors.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define DEFAULT_INTEGER_FORMAT		L"%d"
#define DEFAULT_FLOAT_FORMAT		L"%.4f"
#define DEFAULT_OFFSET_X_INCREMENT	1
#define DEFAULT_OFFSET_Y_INCREMENT	10
#define DEFAULT_SCALE_X_INCREMENT	0.2f
#define DEFAULT_SCALE_Y_INCREMENT	0.005f

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

IMPLEMENT_DYNAMIC(CDlgPlotSettings, CDialogEx)

CDlgPlotSettings::CDlgPlotSettings(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_PLOTSETTINGS, pParent)
{
	m_Plotter		= NULL;
	m_bInitDialog	= true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CDlgPlotSettings::~CDlgPlotSettings()
{
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::AttachToPlotter(CPlotView * inPlotter)
{
	m_Plotter = inPlotter;
	if (NULL == m_Plotter)
	{
		return;
	}

	m_bInitDialog = true;

	// read settings and update controls
	//
	m_Settings.m_ptRefPos.x		= m_Plotter->LineRefX();
	m_Settings.m_ptRefPos.y		= m_Plotter->LineRefY();
	m_Settings.m_ScaleX			= m_Plotter->ScaleX();
	m_Settings.m_ScaleY			= m_Plotter->ScaleY();
	m_Settings.m_ptViewport.x	= m_Plotter->ViewPortX();
	m_Settings.m_ptViewport.y	= m_Plotter->ViewPortY();

	// ---------------------------------------
	//
	CString sValue;

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptRefPos.x);
	m_EdRefPosX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptRefPos.y);
	m_EdRefPosY.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_FLOAT_FORMAT, m_Settings.m_ScaleX);
	m_EdScaleX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_FLOAT_FORMAT, m_Settings.m_ScaleY);
	m_EdScaleY.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptViewport.x);
	m_EdOffsetX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptViewport.y);
	m_EdOffsetY.SetWindowTextW(sValue);

	m_bInitDialog = false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDREF_X, m_EdRefPosX);
	DDX_Control(pDX, IDC_SPIN1, m_SpinX);
	DDX_Control(pDX, IDC_EDREF_Y, m_EdRefPosY);
	DDX_Control(pDX, IDC_SPIN2, m_SpinY);
	DDX_Control(pDX, IDC_EDSCALE_X, m_EdScaleX);
	DDX_Control(pDX, IDC_EDSCALE_Y, m_EdScaleY);
	DDX_Control(pDX, IDC_SPIN_SCALEX, m_SpinScaleX);
	DDX_Control(pDX, IDC_SPIN_SCALEY, m_SpinScaleY);
	DDX_Control(pDX, IDC_EDZERO_X, m_EdOffsetX);
	DDX_Control(pDX, IDC_EDZERO_Y, m_EdOffsetY);
	DDX_Control(pDX, IDC_SPINZEROX, m_SpinOffsetX);
	DDX_Control(pDX, IDC_SPINZEROY, m_SpinOffsetY);
	DDX_Control(pDX, IDC_PICVIEWPORT, m_picViewPort);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CDlgPlotSettings, CDialogEx)
	ON_WM_SHOWWINDOW()
	ON_WM_CTLCOLOR()
	ON_EN_UPDATE(IDC_EDREF_X, &CDlgPlotSettings::OnEnUpdateEdrefX)
	ON_EN_UPDATE(IDC_EDREF_Y, &CDlgPlotSettings::OnEnUpdateEdrefY)
	ON_EN_UPDATE(IDC_EDSCALE_X, &CDlgPlotSettings::OnEnUpdateEdscaleX)
	ON_EN_UPDATE(IDC_EDSCALE_Y, &CDlgPlotSettings::OnEnUpdateEdscaleY)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SCALEX, &CDlgPlotSettings::OnDeltaposSpinScalex)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SCALEY, &CDlgPlotSettings::OnDeltaposSpinScaley)
	ON_BN_CLICKED(IDC_ORIGINAL_REF, &CDlgPlotSettings::OnBnClickedOriginalRef)
	ON_BN_CLICKED(IDC_ORIGINAL_SCALE, &CDlgPlotSettings::OnBnClickedOriginalScale)
	ON_BN_CLICKED(IDC_ORIGINAL_ZERO, &CDlgPlotSettings::OnBnClickedOriginalZero)
	ON_EN_UPDATE(IDC_EDZERO_X, &CDlgPlotSettings::OnEnUpdateEdzeroX)
	ON_EN_UPDATE(IDC_EDZERO_Y, &CDlgPlotSettings::OnEnUpdateEdzeroY)
END_MESSAGE_MAP()

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

BOOL CDlgPlotSettings::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	//	misc
	//
	m_DlgBrushBack.CreateSolidBrush(PapayaWhip_COLOR);

	// set properties for the spin controls
	//
	{
		// accelerators			   secs			increment
		//
		UDACCEL  vAccSpinX[1] = { { 0, DEFAULT_OFFSET_X_INCREMENT } };
		UDACCEL  vAccSpinY[3] = { { 0, DEFAULT_OFFSET_Y_INCREMENT *   1 }, 
								  { 2, DEFAULT_OFFSET_Y_INCREMENT *  10 },
								  { 4, DEFAULT_OFFSET_Y_INCREMENT * 100 } };

		m_SpinX.SetBase(10);
		m_SpinX.SetPos32(0);
		m_SpinX.SetRange32(0, USHORT_MAX);
		m_SpinX.SetBuddy(&m_EdRefPosX);
		m_SpinX.SetAccel(1, vAccSpinX);

		m_SpinY.SetBase(10);
		m_SpinY.SetPos32(0);
		m_SpinY.SetRange32(0, USHORT_MAX);
		m_SpinY.SetBuddy(&m_EdRefPosY);
		m_SpinY.SetAccel(3, vAccSpinY);

		m_SpinScaleX.SetBase(10);
		m_SpinScaleX.SetPos32(0);
		m_SpinScaleX.SetRange32(0, USHORT_MAX);
		m_SpinScaleX.SetBuddy(&m_EdScaleX);

		m_SpinScaleY.SetBase(10);
		m_SpinScaleY.SetPos32(0);
		m_SpinScaleY.SetRange32(0, USHORT_MAX);
		m_SpinScaleY.SetBuddy(&m_EdScaleY);

		m_SpinOffsetX.SetBase(10);
		m_SpinOffsetX.SetPos32(0);
		m_SpinOffsetX.SetRange32(-SHORT_MAX, SHORT_MAX);
		m_SpinOffsetX.SetBuddy(&m_EdOffsetX);

		m_SpinOffsetY.SetBase(10);
		m_SpinOffsetY.SetPos32(0);
		m_SpinOffsetY.SetRange32(-SHORT_MAX, SHORT_MAX);
		m_SpinOffsetY.SetBuddy(&m_EdOffsetY);
	}

	return TRUE;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

HBRUSH CDlgPlotSettings::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (NULL != pDC)
	{
		switch (nCtlColor)
		{
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(DarkGreen_COLOR);
			break;

		case CTLCOLOR_EDIT:
			pDC->SetTextColor(Red_COLOR);
			break;

		case CTLCOLOR_DLG:
		case CTLCOLOR_LISTBOX:
			pDC->SetTextColor(DarkGreen_COLOR);
			break;

		case CTLCOLOR_BTN:
			pDC->SetTextColor(Black_COLOR);
			break;
		}

		pDC->SetBkMode(TRANSPARENT);
	}


	return m_DlgBrushBack;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

int CDlgPlotSettings::GetIntegerFromEdit(const CEdit & inEditCtrl)
{
	CString sValue;

	inEditCtrl.GetWindowTextW(sValue);

	// if there is no text then put a zero
	//
	if (true == sValue.IsEmpty())
	{
		return 1;
	}

	return _wtol(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

float CDlgPlotSettings::GetFloatFromEdit(const CEdit & inEditCtrl)
{
	CString sValue;

	inEditCtrl.GetWindowTextW(sValue);

	// if there is no text then put a zero
	//
	if (true == sValue.IsEmpty())
	{
		return 1;
	}

	return (float)_wtof(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::ReadAndSetRefPoint(void)
{
	if (NULL == m_Plotter)
	{
		return;
	}

	m_Plotter->OverrideRefLine((size_t)GetIntegerFromEdit(m_EdRefPosX), (size_t)GetIntegerFromEdit(m_EdRefPosY));

	// here we play the trick and send an update to our graphics
	//
	CWnd * pWnd = GetParent();

	if (NULL != pWnd)
	{
		pWnd->PostMessage(WM_PAINT);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::ReadAndSetScale(void)
{
	if (NULL == m_Plotter)
	{
		return;
	}

	m_Plotter->OverrideScale(GetFloatFromEdit(m_EdScaleX), GetFloatFromEdit(m_EdScaleY));

	// here we play the trick and send an update to our graphics
	//
	CWnd * pWnd = GetParent();

	if (NULL != pWnd)
	{
		pWnd->PostMessage(WM_PAINT);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::ReadAndSetOffset(void)
{
	if (NULL == m_Plotter)
	{
		return;
	}

	m_Plotter->OverrideViewPort((size_t)GetIntegerFromEdit(m_EdOffsetX), (size_t)GetIntegerFromEdit(m_EdOffsetY));

	// here we play the trick and send an update to our graphics
	//
	CWnd * pWnd = GetParent();

	if (NULL != pWnd)
	{
		pWnd->PostMessage(WM_PAINT);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnEnUpdateEdrefX()
{
	if(false== m_bInitDialog)
		ReadAndSetRefPoint();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnEnUpdateEdrefY()
{
	if (false == m_bInitDialog)
		ReadAndSetRefPoint();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnEnUpdateEdscaleX()
{
	if (false == m_bInitDialog)
	{
		ReadAndSetScale();
		ReadAndSetRefPoint();
		ReadAndSetOffset();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------


void CDlgPlotSettings::OnEnUpdateEdscaleY()
{
	if (false == m_bInitDialog)
	{
		ReadAndSetScale();
		ReadAndSetRefPoint();
		ReadAndSetOffset();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnEnUpdateEdzeroX()
{
	if (false == m_bInitDialog)
	{
		ReadAndSetOffset();
		ReadAndSetRefPoint();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnEnUpdateEdzeroY()
{
	if (false == m_bInitDialog)
	{
		ReadAndSetOffset();
		ReadAndSetRefPoint();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnDeltaposSpinScalex(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	// get the value, add or remove then
	//
	float	fValue = GetFloatFromEdit(m_EdScaleX);
	CString sValue;

	// delta can be -1 or +1
	//
	fValue += (DEFAULT_SCALE_X_INCREMENT * pNMUpDown->iDelta);
	if (DEFAULT_SCALE_X_INCREMENT > fValue)
	{
		fValue = DEFAULT_SCALE_X_INCREMENT;
	}

	// setting the text will cal the update on the entry field
	// which in turn will update the plotter
	//
	sValue.Format(DEFAULT_FLOAT_FORMAT, fValue);
	m_EdScaleX.SetWindowTextW(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnDeltaposSpinScaley(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	*pResult = 0;

	// get the value, add or remove then
	//
	float	fValue = GetFloatFromEdit(m_EdScaleY);
	CString sValue;

	fValue += (DEFAULT_SCALE_Y_INCREMENT * pNMUpDown->iDelta);
	if (DEFAULT_SCALE_Y_INCREMENT > fValue)
	{
		fValue = DEFAULT_SCALE_Y_INCREMENT;
	}

	sValue.Format(DEFAULT_FLOAT_FORMAT, fValue);
	m_EdScaleY.SetWindowTextW(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnBnClickedOriginalRef()
{
	CString sValue;

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptRefPos.x);
	m_EdRefPosX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptRefPos.y);
	m_EdRefPosY.SetWindowTextW(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnBnClickedOriginalScale()
{
	CString sValue;

	sValue.Format(DEFAULT_FLOAT_FORMAT, m_Settings.m_ScaleX);
	m_EdScaleX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_FLOAT_FORMAT, m_Settings.m_ScaleY);
	m_EdScaleY.SetWindowTextW(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgPlotSettings::OnBnClickedOriginalZero()
{
	CString sValue;

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptViewport.x);
	m_EdOffsetX.SetWindowTextW(sValue);

	sValue.Format(DEFAULT_INTEGER_FORMAT, m_Settings.m_ptViewport.y);
	m_EdOffsetY.SetWindowTextW(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

