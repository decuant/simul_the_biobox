// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#include "stdafx.h"
#include "Background.h"
#include "C_Colors.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#define MIN_COLUMMSIZE		5
#define MIN_ROWSIZE			5

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
CBackground::CBackground( void )
{
	m_Dc         = NULL ;

	m_DrawOrigin = true ;
	m_DrawBorder = false ;

	m_ColWidth   = MIN_COLUMMSIZE;
	m_RowHeight  = MIN_ROWSIZE;

	SetClr_Background(LightYellow_COLOR);
	SetClr_GridLines(Black_COLOR);
	SetClr_Reference(Red_COLOR);
	SetClr_Border(DarkViolet_COLOR);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
CBackground::~CBackground( void )
{
	m_PenGrid.DeleteObject( ) ;
	m_PenBorder.DeleteObject( ) ;
	m_PenReference.DeleteObject( ) ;
	m_BrushBack.DeleteObject( ) ;

	DeleteDCs( ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::DeleteDCs( void )
{
	if( NULL != m_Dc )
	{
		m_Dc->DeleteDC( ) ;
		delete m_Dc ;
		m_Dc = NULL ;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::SetWindowRect(const CRect  & rc)
{
	if(m_ClientRect != rc )
	{
		m_ClientRect.CopyRect( rc ) ;

		//	make a copy of the rectangle
		//	this gives the correct offset where to start drawing
		//
		m_DrawRect.CopyRect(m_ClientRect);
		m_DrawRect.OffsetRect(-m_ClientRect.left, -m_ClientRect.top);

		DeleteDCs();
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CBackground::SetColRowSize(size_t inColWidth, size_t inRowHeight)
{
	// set an acceptable minimum
	//
	m_ColWidth  = max(MIN_COLUMMSIZE, (int)inColWidth);
	m_RowHeight = max(MIN_ROWSIZE, (int)inRowHeight);

	DeleteDCs( ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CBackground::SetReference(const CPoint & inPoint)
{
	m_PtReference = inPoint;

	DeleteDCs();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CBackground::SetClr_Background(const COLORREF & inColor)
{
	m_BrushBack.DeleteObject();
	m_BrushBack.CreateSolidBrush(inColor);

	DeleteDCs();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CBackground::SetClr_GridLines(const COLORREF & inColor)
{
	m_PenGrid.DeleteObject();
	m_PenGrid.CreatePen(PS_DOT | PS_COSMETIC | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 1, inColor);

	DeleteDCs();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::SetClr_Reference(const COLORREF & inColor)
{
	m_PenReference.DeleteObject();
	m_PenReference.CreatePen(PS_SOLID | PS_COSMETIC | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 3, inColor);

	DeleteDCs();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::SetClr_Border(const COLORREF & inColor)
{
	m_PenBorder.DeleteObject();
	m_PenBorder.CreatePen(PS_SOLID | PS_COSMETIC | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 2, inColor);

	DeleteDCs();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::InternalDrawGrid( CDC  * inDc )
{
	CPen     * oldPen ;
	int       iOldBkMode ;
	long      x = 0 ;
	long      y = 0 ;


	if( NULL == inDc )
	{
		return ;
	}

	//	paint the background with some color
	//	(not painting will result in a black background)
	//
	inDc->FillRect( (LPRECT)m_DrawRect, & m_BrushBack ) ;

	iOldBkMode = inDc->SetBkMode( TRANSPARENT ) ;
	oldPen     = inDc->SelectObject( & m_PenGrid ) ;

	while( x <= m_DrawRect.right )
	{
		inDc->MoveTo( x, 0 ) ;
		inDc->LineTo( x, m_ClientRect.bottom ) ;

		x += m_ColWidth ;
	}

	while( y <= m_DrawRect.bottom )
	{
		inDc->MoveTo( 0, y ) ;
		inDc->LineTo(m_ClientRect.right, y ) ;

		y += m_RowHeight ;
	}

	if( true == m_DrawBorder )
	{
		//	give it a complete border
		//
		inDc->SelectObject( &m_PenBorder) ;

		inDc->MoveTo( 1, 1 ) ;
		inDc->LineTo( m_DrawRect.right - 1, 1 ) ;
		inDc->LineTo( m_DrawRect.right - 1, m_DrawRect.bottom - 1 ) ;
		inDc->LineTo( 1, m_DrawRect.bottom - 1 ) ;
		inDc->LineTo( 1, 1 ) ;
	}

	if( true == m_DrawOrigin )
	{
		//	draw the reference axis
		//
		inDc->SelectObject(&m_PenReference);

		inDc->MoveTo(m_PtReference.x, 0);
		inDc->LineTo(m_PtReference.x, m_DrawRect.bottom);

		inDc->MoveTo(0, m_PtReference.y);
		inDc->LineTo(m_DrawRect.right, m_PtReference.y);
	}

	//	restore
	//
	inDc->SelectObject( oldPen ) ;
	inDc->SetBkMode( iOldBkMode ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CBackground::Draw( CDC  * inDc )
{
	if( NULL == m_Dc )
	{
		CBitmap     * image = new CBitmap ;

		m_Dc = new CDC ;
		m_Dc->CreateCompatibleDC( inDc ) ;
		m_Dc->SetGraphicsMode( GM_ADVANCED ) ;

		//	uses original dc
		//
		image->CreateCompatibleBitmap( inDc, m_ClientRect.Width( ), m_ClientRect.Height( ) ) ;
		m_Dc->SelectObject( image ) ;

		//	do a one time draw ...
		//
		InternalDrawGrid( m_Dc ) ;

		image->DeleteObject( ) ;
		delete image ;
	}

	inDc->BitBlt(m_ClientRect.left, m_ClientRect.top, m_ClientRect.Width( ), m_ClientRect.Height( ), m_Dc, 0, 0, SRCCOPY ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
