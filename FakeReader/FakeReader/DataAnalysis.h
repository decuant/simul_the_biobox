
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#pragma once
#include "TickCount.h"

//-----------------------------------------------------------------------------
//	class:		CStatData
//-----------------------------------------------------------------------------

class CStatData
{
private:

	long	m_lReference;						// the key
	long	m_lFrequency;						// frequency in data set
	bool	m_bIsSpike;							// is a spike

public:

	CStatData(void)
	{
		m_lReference	= 0L;
		m_lFrequency	= 0L;
		m_bIsSpike		= false;
	}

	CStatData(long inRef, long inFreq = 0)
	{
		m_lReference = inRef;
		m_lFrequency = inFreq;
		m_bIsSpike	 = false;
	}

	// get values
	//
	inline long Reference(void) const { return m_lReference; }
	inline long Frequency(void) const { return m_lFrequency; }
	inline bool IsSpike(void)   const { return m_bIsSpike; }

	// set values
	//
	inline void Frequency(long inFreq)		{ m_lFrequency = inFreq; }
	inline void IsSpike(bool inSpikeFlag)	{ m_bIsSpike   = inSpikeFlag; }
	
	// counting
	//
	inline long IncFrequency(void) { return ++m_lFrequency; }
	inline long DecFrequency(void) { return --m_lFrequency; }

	// all of these operators are necessary because this data will be collected by a CMap<>
	//
	bool operator ==(const CStatData & inStatData) { return m_lReference == inStatData.m_lReference; }
	bool operator !=(const CStatData & inStatData) { return m_lReference != inStatData.m_lReference; }

	bool operator < (const CStatData & inStatData) { return m_lReference < inStatData.m_lReference; }
	bool operator <=(const CStatData & inStatData) { return m_lReference <= inStatData.m_lReference; }

	bool operator > (const CStatData & inStatData) { return m_lReference > inStatData.m_lReference; }
	bool operator >=(const CStatData & inStatData) { return m_lReference >= inStatData.m_lReference; }

};

//-----------------------------------------------------------------------------
//	class:		CDataAnalysis
//-----------------------------------------------------------------------------

#define MAXBARSONCHART		10
#define BACKTASKATTIME		2000

typedef CMap<long, long, CStatData, CStatData>		CStatMap;

class CDataAnalysis
{
private:

	CStatMap	m_mFrequencies;					// map of ref/freq pairs
	CStatMap	m_mSpikes;						// map of spikes
	CTickCount	m_tkBacktask;					// timer for backtask
	bool		m_bEnable;						// enable/disable
	bool		m_bRunning;						// running flag

	size_t		m_stat_numPts;					// total processed
	long		m_stat_min;						// min value
	long		m_stat_max;						// average
	long		m_stat_avg;						// max value

	CTime		m_StartTime;					// record start tme
	CTime		m_StopTime;						// record stop time

	long		m_vBarChart[MAXBARSONCHART];	// bar chart hits

private:

	void Backtask(void);

protected:

	void RefreshBarChart(void);				// update the bar chart

public:

	CDataAnalysis(void);
	~CDataAnalysis(void);

	void Release(void);
	void SetStart(void);
	void SetStop(bool inCArryOn);

	// get values, update values, add values
	//
	bool Lookup(long inIndex, CStatData & outData) const;
	void MapValue(long inNewValue);

	// scan a vector, update analisys
	//
	void ScanPointsVector(const CPoint inVector[], size_t inVectorSize);

	// enable/disable the analyzer
	//
	inline	bool	Enabled(void) const { return m_bEnable; }
			void	Enabled(bool inFlag);

	// getters
	//
	inline bool Running(void) const { return m_bRunning; }

	inline	size_t Value_NumPoints(void) const { return m_stat_numPts; }
	inline	long Value_Minimum(void) const { return m_stat_min; }
	inline	long Value_Maximum(void) const { return m_stat_max; }
	inline	long Value_Average(void) const { return m_stat_avg; }
			long Value_BarChart(size_t inIndex) const;

	inline const CTime & StartTime(void)const { return m_StartTime; }
	inline const CTime & StopTime(void)const  { return m_StopTime; }
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
