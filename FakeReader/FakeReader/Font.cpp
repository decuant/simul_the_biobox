//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "math.h"
#include "font.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	this creates any kind of font, but with points/pixels
//
void SelectPointFont( CDC * inDc, TCHAR * inFacename, int inHeight )
{
	if( ( NULL != inDc ) && ( NULL != inFacename ) )
	{
		CFont      fnPoint ;
		CFont     * fnOld ;

		int        caps = (int)ceil( (float)( GetDeviceCaps( inDc->GetSafeHdc( ), LOGPIXELSY ) / LOGPIXELSY ) ) ;

		fnPoint.CreatePointFont( ( inHeight - caps ) * 10, inFacename, inDc ) ;

		fnOld = inDc->SelectObject( & fnPoint ) ;

		fnOld->DeleteObject( ) ;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	this creates a monospaced font, __ONLY__
//
void MakeFont( CFont * inFont, CDC * inDc, TCHAR * inFacename, int inHeight, bool inBold, int inDegrees )
{
	LOGFONT    logfont ;


	if( ( NULL == inFont ) || ( NULL == inFacename ) || ( NULL == inDc ) )
	{
		return ;
	}

	//	caps must be rounded ...
	//
	int    caps = (int)ceil( (float)( GetDeviceCaps( inDc->GetSafeHdc( ), LOGPIXELSY ) / LOGPIXELSY ) ) ;

	ZeroMemory( logfont.lfFaceName, LF_FACESIZE ) ;
	wcsncpy_s( logfont.lfFaceName, LF_FACESIZE, inFacename, LF_FACESIZE - 1 ) ;

	logfont.lfHeight         = - abs( inHeight ) * caps ;
	logfont.lfWidth          = 0 ;
	logfont.lfEscapement     = 10 * inDegrees ;
	logfont.lfOrientation    = 10 * inDegrees ;
	logfont.lfWeight         = ( inBold ) ? FW_BOLD : FW_NORMAL ;
	logfont.lfItalic         = FALSE ;
	logfont.lfUnderline      = FALSE ;
	logfont.lfStrikeOut      = FALSE ;
	logfont.lfOutPrecision   = OUT_RASTER_PRECIS ;
	logfont.lfClipPrecision  = CLIP_DEFAULT_PRECIS ;
	logfont.lfQuality        = CLEARTYPE_QUALITY ;
	logfont.lfPitchAndFamily = FF_MODERN ; // MONO_FONT | FF_MODERN ;
	logfont.lfCharSet        = ANSI_CHARSET ;

	if( NULL != inFont->m_hObject )                             //	it's an error to recreate a font
	{
		inFont->DeleteObject( ) ;                           //	if it already exists !
	}

	inFont->CreateFontIndirect( & logfont ) ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void SelectThisFont( CDC * inDc, TCHAR * inFacename, int inHeight, bool inBold, int inDegrees )
{
	CFont     * fnNew, * fnOld ;


	if( ( NULL == inDc ) || ( NULL == inFacename ) )
	{
		return ;
	}

	if( NULL != ( fnNew = new CFont( ) ) )
	{
		MakeFont( fnNew, inDc, inFacename, inHeight, inBold, inDegrees ) ;

		fnOld = inDc->SelectObject( fnNew ) ;

		if( NULL != fnOld )
		{
			fnOld->DeleteObject( ) ;
		}

		fnNew->DeleteObject( ) ;
		delete fnNew ;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	this function has been prepared to work with MFC
//	the font returned must be freed when no more in use
//
CFont * SelectThisFont( CWnd * inWindow, TCHAR * inFacename, int inHeight, bool inBold, int inDegrees )
{
	CFont     * fnNew = NULL ;

	if( NULL != inWindow )
	{
		CDC     * dc = inWindow->GetDC( ) ;

		fnNew = new CFont( ) ;
		MakeFont( fnNew, dc, inFacename, inHeight, inBold, inDegrees ) ;
		inWindow->SetFont( fnNew ) ;

		inWindow->ReleaseDC( dc ) ;
	}

	return fnNew ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CalcCharSize( CDC * inDc, stCharSize & ouCalc )
{
	TEXTMETRIC    metrics ;


	ZeroMemory( & metrics, sizeof( metrics ) ) ;

	if( NULL != inDc )
	{
		//	get the width/heigth of a single character
		//
		inDc->GetTextMetrics( & metrics ) ;
	}

	ouCalc.iLineHeigth = metrics.tmHeight ;
	ouCalc.iCharWidth  = metrics.tmAveCharWidth ;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
