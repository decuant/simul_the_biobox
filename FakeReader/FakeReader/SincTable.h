//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

enum eSincFxTypes
{
	eSinc_Fx_Invalid		= -1,
	eSinc_Fx_Original		= 0,       //	this is the default
	eSinc_Fx_Normalized,
	eSinc_Fx_Windowed,

	eSinc_Fx_Max
}; 

//-----------------------------------------------------------------------------
//	class:		CSincTable
//-----------------------------------------------------------------------------

class CSincTable
{
private:

	float			* m_SincTable;
	eSincFxTypes	m_SincType;
	size_t			m_MaxElements;
	size_t			m_TimeIncrement;

private:

	void Release(void);

	//	type of SINC functions
	//
	float  __SincFunction(float x) const;
	float  __NormSincFunction(float x) const;
	float  __WindowedSincFunction(float x) const;

public:

	CSincTable(void);
	~CSincTable(void);

	bool Setup(eSincFxTypes inType, size_t inMaxElements, size_t inTimeInterval);

	//	getters
	//
	float & operator [](UINT inIndex);

	inline size_t MaxElements(void) const   { return m_MaxElements;	}
	inline size_t TimeIncrement(void) const { return m_TimeIncrement; }
};


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
