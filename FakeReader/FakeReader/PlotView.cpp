// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#include "stdafx.h"
#include "PlotView.h"
#include "Font.h"
#include "C_Colors.h"

//-----------------------------------------------------------------------------
//	class:		CPlotView
//-----------------------------------------------------------------------------

CPlotView::CPlotView()
{
	Reset();

	m_Background = new CBackground();

	SetClr_Plot_Dots(DarkSlateBlue_COLOR);
	SetClr_Plot_Segments(Grey80_COLOR);
	SetClr_Plot_SincFx(MediumSeaGreen_COLOR);
	SetClr_Plot_Hits(Salmon_COLOR, Orchid_COLOR);
	SetClr_Plot_Bars(Grey20_COLOR);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CPlotView::~CPlotView()
{
	if (NULL != m_Background)
	{
		delete m_Background;
	}

	DeleteAllPens();
	DeleteDCs(DELETE_DC_ALL);

	m_Analisys.Release();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::Reset(void)
{
	m_DcDraw		= NULL;            
	m_Background	= NULL;

	m_ColWidth		= SIZE_GRID_COLUMNS;
	m_RowHeight		= SIZE_GRID_ROWS;

	m_BrushDot		= NULL;
	m_PenChannel1	= NULL;
	m_PenChannel2	= NULL;
	m_PenSinc		= NULL;
	m_BrushHit		= NULL;
	m_PenHit		= NULL;
	m_BrushBars		= NULL;

	m_vPoints		= NULL;
	m_PointsNumber  = 0;

	m_fScaleX		= 0.0f;
	m_fScaleY		= 0.0f;

	m_ptViewport.SetPoint(0, 0);
	m_ptRefPos.SetPoint(0, 0);

	m_eDrawFunction = eDraw_Fx_Disabled;

	m_Analisys.Enabled(false);
	m_fYScale		= 0.0f;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetStartTime(void)
{
	m_Analisys.SetStart();

	// this is used by the drawing function to adapt the scaling
	// must clear it between sessions
	//
	m_fYScale = 10.0f * (float)m_RectDraw.Height() / (float)(SHORT_MAX);	// 0.013367

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetStopTime(void)
{
	m_Analisys.SetStop(false);

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::EnableAnalisys(bool inFlag)
{
	m_Analisys.Enabled(inFlag);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool CPlotView::IsAnalisysEnabled(void) const
{
	return m_Analisys.Enabled();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::ReleaseGDIObj(CGdiObject * inWhich)
{
	if (NULL != inWhich)
	{
		inWhich->DeleteObject();
		delete inWhich;
		inWhich = NULL;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Plot_Dots(const COLORREF & inColor)
{
	ReleaseGDIObj(m_BrushDot);

	m_BrushDot = new CBrush();
	m_BrushDot->CreateSolidBrush(inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Plot_Hits(const COLORREF & inBrushClr, const COLORREF & inPenClr)
{
	ReleaseGDIObj(m_BrushHit);
	ReleaseGDIObj(m_PenHit);

	m_BrushHit = new CBrush();
	m_BrushHit->CreateSolidBrush(inBrushClr);

	m_PenHit = new CPen(PS_COSMETIC | PS_SOLID | PS_USERSTYLE | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 5, inPenClr);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Plot_Segments(const COLORREF & inColor)
{
	ReleaseGDIObj(m_PenChannel1);
	ReleaseGDIObj(m_PenChannel2);

	m_PenChannel1 = new CPen(PS_COSMETIC | PS_SOLID | PS_USERSTYLE | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 10, inColor);
	m_PenChannel2 = new CPen(PS_COSMETIC | PS_SOLID | PS_USERSTYLE | PS_ENDCAP_FLAT | PS_JOIN_BEVEL,  5, ~inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Plot_SincFx(const COLORREF & inColor)
{
	ReleaseGDIObj(m_PenSinc);

	m_PenSinc = new CPen(PS_COSMETIC | PS_SOLID | PS_USERSTYLE | PS_ENDCAP_FLAT | PS_JOIN_BEVEL, 2, inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Plot_Bars(const COLORREF & inBrushClr)
{
	ReleaseGDIObj(m_BrushBars);

	m_BrushBars = new CBrush();
	m_BrushBars->CreateSolidBrush(inBrushClr);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::DeleteAllPens(void)
{
	ReleaseGDIObj(m_PenChannel1);
	ReleaseGDIObj(m_PenChannel2);
	ReleaseGDIObj(m_PenSinc);
	ReleaseGDIObj(m_BrushDot);
	ReleaseGDIObj(m_BrushHit);
	ReleaseGDIObj(m_PenHit);
	ReleaseGDIObj(m_BrushBars);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Back_Background(const COLORREF & inColor)
{
	if (NULL != m_Background)
		m_Background->SetClr_Background(inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Back_GridLines(const COLORREF & inColor)
{
	if (NULL != m_Background)
		m_Background->SetClr_GridLines(inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Back_Reference(const COLORREF & inColor)
{
	if (NULL != m_Background)
		m_Background->SetClr_Reference(inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetClr_Back_Border(const COLORREF & inColor)
{
	if (NULL != m_Background)
		m_Background->SetClr_Border(inColor);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InitializeMemory(size_t inNumPoints, size_t inTimeSpan)
{
	// check modification
	//
	if (m_PointsNumber != inNumPoints)
	{
		if (NULL != m_vPoints)
		{
			delete[] m_vPoints;
			m_vPoints = NULL;
		}

		// allocate necessary space
		//
		m_PointsNumber	= inNumPoints;
		m_vPoints		= new CPoint[m_PointsNumber];

		if (0 < inNumPoints)
		{
			// let update the scale values
			//
			RecalcScale();

			//	get the precompiled sinc table
			//
			size_t uTimeIncrement = inTimeSpan;
			size_t uTimeLimit	  = uTimeIncrement * (m_RectDraw.Width() / 1);

			// eSinc_Fx_Original
			// eSinc_Fx_Normalized
			// eSinc_Fx_Windowed

			m_SincTable.Setup(eSinc_Fx_Normalized, uTimeLimit, uTimeIncrement);
		}

		// prepare memory for the statistics
		//
		m_Analisys.Release();
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::UpdateData(const CPoint inPoints[], size_t inNumPoints)
{
//	if (NULL != m_vPoints && NULL != inPoints)
	{
		for (size_t kk = 0; kk < m_PointsNumber && kk < inNumPoints; kk++)
		{
			m_vPoints[kk] = inPoints[kk];
		}

		// perform addtions here
		//
		if (true == m_Analisys.Enabled())
		{
			m_Analisys.ScanPointsVector(m_vPoints, m_PointsNumber);
		}
	}

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::SetDrawingFunction(eDrawFunction inValue)
{
	m_eDrawFunction = inValue;
	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::ForceRefresh(void)
{
	eDrawFunction oldValue = m_eDrawFunction;

	m_eDrawFunction = eDraw_Fx_Disabled;
	
	DeleteDCs(DELETE_DC_ALL);

	// reset the internal storage
	//
	for (size_t ki = 0; ki < m_PointsNumber; ki++)
	{
		m_vPoints[ki].SetPoint(0, 0);
	}

	m_eDrawFunction = oldValue;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::DeleteDCs(DWORD inFlag)
{
	if (eDraw_Fx_Hitmap == m_eDrawFunction)
	{
		return;
	}

	if ((0 != (inFlag & DELETE_DC_DRAW)) && (NULL != m_DcDraw))
	{
		m_DcDraw->DeleteDC();
		delete m_DcDraw;
		m_DcDraw = NULL;
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CPlotView::SetWindowRect(const CRect & inRect)
{
	m_RectDraw = inRect;

	//	set the background grid spacing
	//
	if (NULL != m_Background)
	{
		CRect    rcTemp(CPoint(0, 0), CSize(m_RectDraw.Width(), m_RectDraw.Height()));

		m_Background->SetWindowRect(rcTemp);

		// get the new scaling factor
		//
		RecalcScale();
	}

	//	force a complete redraw of the graphics
	//
	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	map the point using the gridd coords, the general zoom factor
//	and the individual setting for line zooms
//
void CPlotView::MapToOriginEx(CPoint & ioPoint)
{
	//	calc the X value
	//
	{
		ioPoint.x = (long)ceil(((float)ioPoint.x * m_fScaleX));
	}

	//	calc the Y value
	//
	{
		ioPoint.y = m_RectDraw.Height() - (long)(ceil((float)ioPoint.y * m_fScaleY));
	}

	// shift, to the left and up, a bit
	//
	//ioPoint.x +=  5;
	//ioPoint.y -=  5;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	map the point using the gridd coords, the general zoom factor
//	and the individual setting for line zooms
//
void CPlotView::MapToOriginSinc(CPoint & ioPoint)
{
	//	calc the X value
	//
	{
		ioPoint.x = (long)ceil(((float)ioPoint.x * m_fScaleX));
	}

	//	calc the Y value
	//
	{
		ioPoint.y = m_RectDraw.Height() - (long)(ceil((float)ioPoint.y * m_fScaleY));
	}

	if (0 <= ioPoint.y)
	{
		ioPoint.y -= (m_RectDraw.Height() / 2);
	}
	else
	{
		ioPoint.y *= -1;
		ioPoint.y -= (m_RectDraw.Height());
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::RecalcScale(void)
{
	float fOldX = m_fScaleX;
	float fOldY = m_fScaleY;

	m_fScaleX = (float)m_RectDraw.Width() / (float)m_PointsNumber;
	m_fScaleY = (float)m_RectDraw.Height() / (float)USHORT_MAX;

	// if something has changed redraw the background
	//
	if (fOldX != m_fScaleX || fOldY != m_fScaleY)
	{
		size_t uNewWidth = (size_t)ceil((float)m_ColWidth * m_fScaleX);
		size_t uNewHeight = (size_t)ceil((float)m_RowHeight * m_fScaleY);

		m_Background->SetColRowSize(uNewWidth, uNewHeight);
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::OverrideRefLine(size_t inOffsetX, size_t inOffsetY)
{
	m_ptRefPos.x = (long)inOffsetX;
	m_ptRefPos.y = (long)inOffsetY;

	// object lays on the background
	// tbd move to plotter or make a class
	//
	if (NULL != m_Background)
	{
		CPoint ptMappedRef(m_ptRefPos);

		MapToOriginEx(ptMappedRef);
		m_Background->SetReference(ptMappedRef);
	}

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::OverrideScale(const float & inScaleX, const float & inScaleY) 
{
	m_fScaleX = inScaleX;
	m_fScaleY = inScaleY;

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::OverrideViewPort(size_t inOffsetX, size_t inOffsetY)
{
	m_ptViewport.x = (long)inOffsetX;
	m_ptViewport.y = (long)inOffsetY;

	Invalidate();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::Draw(CDC  * inDc)
{
	if (NULL == m_DcDraw)
	{
		CBitmap     * image = new CBitmap;

		m_DcDraw = new CDC();
		m_DcDraw->CreateCompatibleDC(inDc);
		m_DcDraw->SetGraphicsMode(GM_ADVANCED);

		//	uses original dc ...
		//
		image->CreateCompatibleBitmap(inDc, m_RectDraw.Width(), m_RectDraw.Height());
		m_DcDraw->SelectObject(image);

		InternalDrawDrawing(m_DcDraw);

		image->DeleteObject();
		delete image;
	}

	//	the drawing
	//
	inDc->BitBlt(m_RectDraw.left, m_RectDraw.top, m_RectDraw.Width(), m_RectDraw.Height(), m_DcDraw, 0, 0, SRCCOPY);

	// this will overwrite a valid dc
	//
	if (eDraw_Fx_Hitmap == m_eDrawFunction)
	{
		InternalDrawHitMap(m_DcDraw);
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InternalDrawDrawing(CDC  * inDc)
{
	//	we must have pens and colors defined
	//
	if ((NULL == m_Background))
	{
		return;
	}

	// little hack because we don't want the grid lines to be drawn
	//
	if (eDraw_Fx_Sinc >= m_eDrawFunction)
	{
		//	draw the background
		//
		m_Background->Draw(inDc);
	}
	
	// match the function we will use to perform the drawing
	//
	switch (m_eDrawFunction)
	{
	case eDraw_Fx_Disabled:
		break;

	case eDraw_Fx_DotsOnly:

		InternalDrawDotsOnly(inDc);
		break;

	case eDraw_Fx_Segments:

		InternalDrawOneLine(inDc);
		break;

	//case eDraw_Fx_Bezier:

	//	InternalDrawBezier(inDc);
	//	break;

	case eDraw_Fx_Sinc:							// after sinc not drawing the background

		InternalDrawOneLineSincFx(inDc);
		break;

	case eDraw_Fx_Hitmap:					
		break;

	case eDraw_Fx_Analisys:

		InternalDrawAnalisys(inDc);
		InternalDrawOneLine(inDc);
		break;

	default:
		break;
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InternalDrawDotsOnly(CDC * inDc)
{
	CPoint			curPoint;
	CRect			theRect;
	const USHORT	iRectHalfSize	= 5;
	CPen			* oldPen		= inDc->SelectObject(m_PenSinc);

	for (int y = 0; y < (int)m_PointsNumber; y++)
	{
		curPoint = m_vPoints[y];
		MapToOriginEx(curPoint);

		//	if any of the points falls into the visible rectangle then display it
		//
		if (TRUE == inDc->PtVisible(curPoint))
		{
			theRect.left	= max(curPoint.x - iRectHalfSize, 0);
			theRect.right	= curPoint.x + iRectHalfSize;
			theRect.top		= curPoint.y - iRectHalfSize;
			theRect.bottom	= curPoint.y + iRectHalfSize;

			inDc->Rectangle(&theRect);
			theRect.InflateRect(-2, -2);
			inDc->FillRect(&theRect, m_BrushDot);
		}
	}

	inDc->SelectObject(oldPen);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InternalDrawHitMap(CDC * inDc)
{
	CPoint			curPoint;
	CRect			theRect;
	const USHORT	iRectHalfSize	= (USHORT)(ceil(m_fScaleX / 2.0f));
	CPen			* oldPen		= inDc->SelectObject(m_PenHit);

	int oldRop = inDc->GetROP2();

	for (int y = 0; y < (int)m_PointsNumber; y++)
	{
		curPoint = m_vPoints[y];
		MapToOriginEx(curPoint);

		//	if any of the points falls into the visible rectangle then display it
		//
		if (TRUE == inDc->PtVisible(curPoint))
		{
			theRect.left = curPoint.x - iRectHalfSize;
			theRect.right = curPoint.x + iRectHalfSize;
			theRect.top = curPoint.y - iRectHalfSize;
			theRect.bottom = curPoint.y + iRectHalfSize;

			inDc->SetROP2(R2_MERGEPENNOT);
			inDc->Rectangle(&theRect);
			inDc->SetROP2(oldRop);

			theRect.InflateRect(-2, -2);
			inDc->FillRect(&theRect, m_BrushHit);
		}
	}

	inDc->SelectObject(oldPen);
	inDc->SetROP2(oldRop);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#define LABEL_FONT_SIZE			22
#define LINE_SPACING_PIXELS		22 - 10

void CPlotView::InternalDrawAnalisys(CDC * inDc)
{
	// prepare
	//
	inDc->SetTextColor(LightGoldenrodYellow_COLOR);
	inDc->SetBkMode(TRANSPARENT);
	SelectThisFont(inDc, L"Courier New", LABEL_FONT_SIZE, false);

	// calc the text height
	//
	CRect rcExt;
	inDc->DrawText(L"0123456789", &rcExt, DT_LEFT | DT_VCENTER | DT_CALCRECT);

	// this is the range of the columns
	//
	size_t	uInterval = (size_t)ceil((float)m_Analisys.Value_Maximum() / (float)MAXBARSONCHART);

	// values
	//
	CString		sText;
	CRect		rcDraw(8, rcExt.Height() / 2, 500, 500);
	CStatData	cData;
	CBrush		brBarStats(Grey5_COLOR);

	inDc->FillRect(&rcDraw, &brBarStats);

	sText.Format(L"Hits [%2ld]", m_Analisys.Value_NumPoints(), uInterval);
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	// MIN / AVG / MAX
	//
	m_Analisys.Lookup(m_Analisys.Value_Minimum(), cData);
	sText.Format(L"Min  [%6ld] [%6ld]", cData.Reference(), cData.Frequency());

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	m_Analisys.Lookup(m_Analisys.Value_Average(), cData);
	sText.Format(L"Avg  [%6ld] [%6ld]", cData.Reference(), cData.Frequency());

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	m_Analisys.Lookup(m_Analisys.Value_Maximum(), cData);
	sText.Format(L"Max  [%6ld] [%6ld]", cData.Reference(), cData.Frequency());

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;

	sText.Format(L"Start time [%02d:%02d:%02d]", m_Analisys.StartTime().GetHour(), m_Analisys.StartTime().GetMinute(), m_Analisys.StartTime().GetSecond());
	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	// update the stop time
	//
	if (true == m_Analisys.Running())
	{
		m_Analisys.SetStop(true);
	}

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;

	if (false == m_Analisys.Running())
	{
		sText.Format(L"Stop time  [%02d:%02d:%02d]", m_Analisys.StopTime().GetHour(), m_Analisys.StopTime().GetMinute(), m_Analisys.StopTime().GetSecond());
		inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);
	}

	// update the stop time
	//
	CTimeSpan tmTmp = m_Analisys.StopTime() - m_Analisys.StartTime();

	{
		CString szAppend;

		szAppend.Format(L" [%02d:%02d:%02d]", tmTmp.GetHours(), tmTmp.GetMinutes(), tmTmp.GetSeconds());

		if (true == m_Analisys.Running())
		{
			sText = L" +  R U N ";
		}
		else
		{
			sText = L"   S T O P";
		}

		sText += szAppend;
	}

	rcDraw.top += rcExt.Height() + LINE_SPACING_PIXELS;
	inDc->DrawText(sText, &rcDraw, DT_LEFT | DT_VCENTER);

	// -----------------------------------------------
	// drawing the bars here
	//

#define BAR_INTERLEAVE 10

	size_t	kk;				// keep this out of the foor loop declaration!
	CPoint	ptLeft;			// where to draw
	size_t  uAreaAvail  = (m_RectDraw.Width() / 2) + 300;
	long	lBarWidth	= ((long)uAreaAvail - BAR_INTERLEAVE * MAXBARSONCHART) / MAXBARSONCHART;

	CBrush	brBlackness(Black_COLOR);
	CBrush	brBarHigh(Grey20_COLOR);

	inDc->SetTextColor(White_COLOR);

_adapt:

	// see SetStartTime for init of the scaling
	//
	
	// set the starting point on the drawing
	//
	kk		 = 0;
	ptLeft.x = m_RectDraw.Width() - (long)uAreaAvail;

	rcDraw.SetRect(ptLeft.x, 0, m_RectDraw.Width(), m_RectDraw.Height());
	inDc->FillRect(&rcDraw, &brBlackness);

	for (; kk < MAXBARSONCHART; kk++)
	{
		// get the point's height
		//
		ptLeft.y = (long)(m_Analisys.Value_BarChart(kk) * m_fYScale);

		// when too close to the top
		//
		if ((m_RectDraw.Height() / 9) > (m_RectDraw.Height() - ptLeft.y))
		{
			m_fYScale *= 0.7500f;

			// must clear all the area !
			//
			goto _adapt;
		}

		// make a background for the bar
		//
		rcDraw.SetRect(ptLeft.x, m_RectDraw.Height(), ptLeft.x + lBarWidth, m_RectDraw.Height() - ptLeft.y);
		inDc->FillRect(&rcDraw, &brBarHigh);

		// draw over the bar now
		//
		rcDraw.InflateRect(-3, -4);
		inDc->FillRect(&rcDraw, m_BrushBars);
		
		// label: column interval
		//
		rcDraw.SetRect(ptLeft.x, m_RectDraw.Height() - rcExt.Height(), ptLeft.x + lBarWidth, (m_RectDraw.Height() - rcExt.Height()) * 2);

		sText.Format(L"%02d", (uInterval * kk) + uInterval);
		inDc->DrawText(sText, &rcDraw, DT_CENTER | DT_VCENTER);

		// label: sum of frequencies
		//
		rcDraw.top -= rcExt.Height() * 4;

		sText.Format(L"%02d", m_Analisys.Value_BarChart(kk));
		inDc->DrawText(sText, &rcDraw, DT_CENTER | DT_VCENTER);

		// get the next point
		//
		ptLeft.x += (lBarWidth + BAR_INTERLEAVE);
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InternalDrawOneLine(CDC * inDc)
{
	CPoint		curPoint;
	CPoint		prevPoint;

	CPen		* oldPen	= inDc->SelectObject(m_PenChannel1);

	//	here draw connecting line
	//
	curPoint = m_vPoints[0];
	MapToOriginEx(curPoint);
		
	inDc->MoveTo(curPoint);

	for (WORD y = 1; y < m_PointsNumber; y++)
	{
		//	store the current point
		//
		prevPoint = curPoint;

		// work out the next one
		//
		curPoint = m_vPoints[y];
		MapToOriginEx(curPoint);

		// perform the draw
		//
		inDc->SelectObject(m_PenChannel1);
		inDc->LineTo(curPoint);

		inDc->SelectObject(m_PenChannel2);
		inDc->MoveTo(prevPoint);
		inDc->LineTo(curPoint);
	}

	inDc->SelectObject(oldPen);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//#define BEZIER_SEGMENTS              4
//#define BEZIER_COEFF                 2.98500f
//
//void CPlotView::InternalDrawBezier(CDC * inDc)
//{
//	CPoint	curPoint;
//	CPoint	prevPoint;
//	CPoint	bezierArray[BEZIER_SEGMENTS];
//	CPoint	ctrlArray[4];
//
//	CPen	* oldPen = inDc->SelectObject(m_PenChannel1);
//
//	//	here draw connecting line
//	//
//	for (size_t j = 0; (j + 3) < m_PointsNumber; j += 4)
//	{
//		//	load new control handles
//		//
//		for (size_t z = 0; z < 4; z++)
//		{
//			ctrlArray[z].x = m_vPoints[j + z].x;
//			ctrlArray[z].y = m_vPoints[j + z].y;
//		}
//
//		//	enumerate the bezier points
//		//
//		for (int i = 0, k = 0; i < BEZIER_SEGMENTS; i++, k++)
//		{
//			__Bezier4(ctrlArray, bezierArray[k], double(i) / BEZIER_SEGMENTS);
//		}
//
//		//	draw the bezier points here on
//		//
//		curPoint = bezierArray[0];
//		MapToOriginEx(curPoint);
//
//		//	connect the previous end with this segment
//		//
//		if (j > 0)
//		{
//			inDc->MoveTo(prevPoint);
//			inDc->LineTo(curPoint);
//		}
//
//		//	draw all the segments of the curve
//		//
//		for (size_t yy = 0; yy < BEZIER_SEGMENTS; yy++)
//		{
//			prevPoint = curPoint;
//
//			curPoint = bezierArray[yy];
//			MapToOriginEx(curPoint);
//			inDc->LineTo(curPoint);
//		}
//
//		//	need to store this too ...
//		//
//		prevPoint = curPoint;
//	}
//
//	inDc->SelectObject(oldPen);
//}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
//void CPlotView::__Bezier4(const CPoint inPoints[], CPoint & ouPoint, double mu) const
//{
//	double	mum1, mum13, mu2, mu3;
//
//	mum1	= 1 - mu;
//	mum13	= mum1 * mum1 * mum1;
//	mu2		= mu * mu;
//	mu3		= mu2 * mu;
//
//	ouPoint.x = long(mum13 * inPoints[0].x + BEZIER_COEFF * mu * mum1 * mum1 * inPoints[1].x + BEZIER_COEFF * mu2 * mum1 * inPoints[2].x + mu3 * inPoints[3].x);
//	ouPoint.y = long(mum13 * inPoints[0].y + BEZIER_COEFF * mu * mum1 * mum1 * inPoints[1].y + BEZIER_COEFF * mu2 * mum1 * inPoints[2].y + mu3 * inPoints[3].y);
//}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CPlotView::InternalDrawOneLineSincFx(CDC * inDc)
{
	size_t	uTimeIncrement  = m_SincTable.TimeIncrement();			// align with the sinc table
	size_t	uTimeLimit		= m_SincTable.MaxElements();
	CPoint	* vSincPoints	= new CPoint[uTimeLimit];
	CPen	* oldPen		= inDc->SelectObject(m_PenSinc);

	// ----------------------------------------
	//	do the drawing here
	//
	for (size_t y = 0; y < m_PointsNumber / 1; y++)
	{
		size_t		uTimeCount;
		size_t		uIndex;					// loops all points at time interval

		uIndex		= 0;
		uTimeCount	= 0;

		//	fill in all points for the drawing
		//	starts from 0, counts the x difference between the current point and the generated point
		//
		for (; uTimeCount < uTimeLimit && uIndex < m_PointsNumber; uTimeCount += uTimeIncrement, uIndex++)
		{
			int		iDifference;
			float	fCurValue;

			//	get the time offset from current
			//
			iDifference = (int)(uTimeCount - m_vPoints[y].x);
			fCurValue	= 0.0F;

			// experimental test, try to limit the number of lines just to the most
			// adiacent ones, this a performance optimization
			//
			if (16 > abs(iDifference))
			{
				//	check if we are laying on the point itself
				//
				if (0 != iDifference)
				{
					int	iTableIndex = iDifference * (int)uTimeIncrement;

					// get the value of the point * entry in sinc table
					//
					if (0 > iDifference)
					{
						fCurValue = (float)m_vPoints[y].y * (-m_SincTable[-iTableIndex]);
					}
					else
					{
						fCurValue = (float)m_vPoints[y].y * m_SincTable[iTableIndex];
					}
				}
			}

			//	assign to new generated video points
			//
			vSincPoints[uIndex].x = (long)uTimeCount;
			vSincPoints[uIndex].y = (long)(fCurValue);
		}

		//	----------------
		//	do the draw here
		//
		if (1 < uIndex)
		{
			uTimeCount	= uIndex;
			uIndex		= 0;

			MapToOriginSinc(vSincPoints[uIndex]);
			inDc->MoveTo(vSincPoints[uIndex]);

			//	do the rest of the drawing here
			//
			while (++uIndex < (size_t)uTimeCount)
			{
				MapToOriginSinc(vSincPoints[uIndex]);
				inDc->LineTo(vSincPoints[uIndex]);
			}
		}
	}

	delete[] vSincPoints;

	inDc->SelectObject(oldPen);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
