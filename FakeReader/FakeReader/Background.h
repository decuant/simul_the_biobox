// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#pragma once

//-----------------------------------------------------------------------------
//	class:		CVBackground
//-----------------------------------------------------------------------------

class CBackground
{
private:

	CDC			* m_Dc ;
	CPen		m_PenGrid ;
	CPen		m_PenBorder ;
	CPen		m_PenReference ;
	CBrush		m_BrushBack ;

	CRect		m_ClientRect;
	CRect		m_DrawRect ;
	int			m_ColWidth ;
	int			m_RowHeight ;

	CPoint		m_PtReference;
	bool		m_DrawOrigin ;
	bool		m_DrawBorder ;

private:

	void  DeleteDCs( void ) ;
	void  InternalDrawGrid( CDC  * dc ) ;

public:

	CBackground( void ) ;
	~CBackground( void ) ;

	void  SetWindowRect( const CRect  & rc ) ;
	void  SetColRowSize( size_t inColWidth, size_t inRowHeight ) ;

	void  Draw( CDC  * inDc ) ;

	void SetClr_Background(const COLORREF & inColor);
	void SetClr_GridLines(const COLORREF & inColor);
	void SetClr_Reference(const COLORREF & inColor);
	void SetClr_Border(const COLORREF & inColor);

	inline void SetDrawOrigin( bool inValue ) {	m_DrawOrigin = inValue ; }
	inline void SetDrawBorder( bool inValue ) {	m_DrawBorder = inValue ; }
	
	void  SetReference(const CPoint & inPoint);
	inline const CPoint & GetReference(void) { return m_PtReference; }
} ;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
