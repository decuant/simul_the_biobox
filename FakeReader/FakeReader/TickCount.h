//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
#pragma once

//-----------------------------------------------------------------------------
//	class:		CTickCount
//-----------------------------------------------------------------------------

class CTickCount
{
private:

	DWORD    m_NextTick ;                           //	next time to fire
	DWORD    m_TickFrame ;                          //	firing delay
	bool     m_Enabled ;                            //	timer is actually m_Enabled

public:

	CTickCount( void ) ;
	CTickCount( DWORD _tickFrame ) ;
	~CTickCount( void ) {}

	bool operator ==( const CTickCount & inCounter ) const ;
	CTickCount & operator =( const CTickCount & inCounter ) ;

	//	setters
	//
	__forceinline void  Setup( DWORD dw ) { m_NextTick = GetTickCount( ) + ( m_TickFrame = dw ) ; }
	__forceinline void  Reset( void )     { m_NextTick = GetTickCount( ) + m_TickFrame ; }
	__forceinline void  Enabled( bool b ) { m_Enabled = b ; }

	//	getters
	//
	__forceinline bool  Enabled( void ) const { return m_Enabled ; }
	__forceinline bool  Fired( void ) const   { return m_Enabled ? GetTickCount( ) >= m_NextTick : false  ;  }
	__forceinline DWORD Frame( void ) const   { return m_TickFrame ; }
	__forceinline DWORD Elapsed( void ) const { return m_NextTick - GetTickCount( ) ; }
} ;



//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
