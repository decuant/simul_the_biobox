//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by FakeReader.rc 
//
#define IDM_ABOUTBOX                    0x0010
#define IDM_STARTTIMING                 0x0020
#define IDM_STOPTIMING                  0x0030
#define IDM_EDITSETTINGS                0x0040
#define IDM_EDITPARAMETERS              0x0050
#define IDD_FAKEREADER_DIALOG           102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDD_DLG_BIOTEL                  132
#define IDD_DLG_PLOTSETTINGS            134
#define IDD_ABOUTBOX		            135

#define IDC_EDFILENAME                  1000
#define IDC_EDITPORT                    1002
#define IDSTARTTIMER                    1004
#define IDC_EDLOGGING                   1005
#define IDC_PROGRESS1                   1006
#define IDC_LSFILENAME                  1007
#define IDC_BTNADDFILE                  1008
#define IDC_CHKCICLIC                   1009
#define IDC_EDLOCALPORT                 1010
#define IDC_TXTFILEPROPS                1011
#define IDC_BTNDELRECENT                1012
#define IDC_CHKLIVEDATA                 1013
#define IDC_WINPLOT                     1014
#define IDC_CBDRAWTYPE                  1015
#define IDC_CBDRAWCHANNEL               1016
#define IDC_BTN_SEEK_FIRST              1017
#define IDC_BTN_SEEK_LAST               1018
#define IDC_ED_SEEK_VALUE               1019
#define IDC_BTN_SEEK_PREV               1020
#define IDC_BTN_SEEK_NEXT               1021
#define IDC_SPIN1                       1024
#define IDC_EDREF_X                     1025
#define IDC_SPIN2                       1026
#define IDC_EDREF_Y                     1027
#define IDC_SPIN_SCALEX                 1028
#define IDC_EDSCALE_X                   1029
#define IDC_SPIN_SCALEY                 1030
#define IDC_EDSCALE_Y                   1031
#define IDC_ORIGINAL_REF                1032
#define IDC_EDPARAM1					1033
#define IDC_EDPARAM2				    1034
#define IDC_EDPARAM3					1035

#define IDC_EDZERO_X					1036
#define IDC_EDZERO_Y					1037
#define IDC_SPINZEROX					1038
#define IDC_SPINZEROY					1039
#define IDC_PICVIEWPORT					1040
#define IDC_CBBIOTEL					1041

#define IDC_ORIGINAL_SCALE				1042
#define IDC_ORIGINAL_ZERO				1043
#define IDC_LBLTIMESAT					1044
			
#define IDS_ABOUTBOX					1045

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1033
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
