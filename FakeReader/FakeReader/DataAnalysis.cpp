//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "DataAnalysis.h"

//-----------------------------------------------------------------------------
//	class:		CDataAnalysis
//-----------------------------------------------------------------------------

CDataAnalysis::CDataAnalysis(void)
{
	Release();

	m_bEnable = false;			// never autoreset the enable (.ini file)
		
	m_tkBacktask.Setup(BACKTASKATTIME);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CDataAnalysis::~CDataAnalysis(void)
{
	m_mFrequencies.RemoveAll();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::Release(void)
{
	for (int kk = 0; kk < MAXBARSONCHART; kk++)
	{
		m_vBarChart[kk] = 0L;
	}

	m_mFrequencies.RemoveAll();

	m_stat_numPts	= 0;
	m_stat_min		= 0;
	m_stat_max		= 0;
	m_stat_avg		= 0;

	m_bRunning		= false;

	m_StartTime		= CTime::GetCurrentTime();		// record start tme
	m_StopTime		= CTime::GetCurrentTime();		// record stop time
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::SetStart(void)
{
	Release();

	m_bRunning	= true;
	m_StartTime = CTime::GetCurrentTime();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::SetStop(bool inCArryOn)
{
	m_StopTime = CTime::GetCurrentTime();

	if (false == inCArryOn)
	{
		m_bRunning = false;
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::Enabled(bool inFlag)
{ 
	m_bEnable = inFlag; 
	m_tkBacktask.Enabled(m_bEnable);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::MapValue(long inNewValue)
{
	CStatData	cData(inNewValue, 0);

	if (TRUE == m_mFrequencies.Lookup(inNewValue, cData))
	{
		cData.IncFrequency();
	}
	else
	{
		cData.Frequency(1);
	}
	// will update and existing item
	// or will insert a new item
	//
	m_mFrequencies.SetAt(inNewValue, cData);
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

bool CDataAnalysis::Lookup(long inIndex, CStatData & outData) const
{
	return (TRUE == m_mFrequencies.Lookup(inIndex, outData));
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

long CDataAnalysis::Value_BarChart(size_t inIndex) const
{
	if (MAXBARSONCHART > inIndex)
	{
		return m_vBarChart[inIndex];
	}

	return m_vBarChart[MAXBARSONCHART - 1];
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDataAnalysis::ScanPointsVector(const CPoint inVector[], size_t inVectorSize)
{
	if (false == m_bEnable || false == m_bRunning)
	{
		return;
	}


	long	lCurVal;
	long	lAverage = 0;

	for (size_t kk = 0; kk < inVectorSize; kk++)
	{
		// get the item and perform add/update
		//
		lCurVal = inVector[kk].y;
		MapValue(lCurVal);

		// update common stats
		//
		if (0 == m_stat_numPts)
		{
			m_stat_max = m_stat_min = lCurVal;
		}

		m_stat_numPts++;
		lAverage += lCurVal;

		if (lCurVal > m_stat_max)
			m_stat_max = lCurVal;

		if (lCurVal < m_stat_min)
			m_stat_min = lCurVal;
	}

	// make the average
	//
	lAverage += m_stat_avg;
	lAverage /= (long)(inVectorSize + 1);

	m_stat_avg = lAverage;

	// check if we have to run for something more
	//
	if (true == m_tkBacktask.Fired())
	{
		Backtask();

		m_tkBacktask.Reset();
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CDataAnalysis::Backtask(void)
{
//	TRACE(L"Backtask runs here\n");

	RefreshBarChart();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void CDataAnalysis::RefreshBarChart(void)
{
	if (false == m_bEnable || false == m_bRunning)
	{
		return;
	}

	if (0 >= m_stat_max)
	{
		return;
	}

	// reset old entries
	//
	for (int kk = 0; kk < MAXBARSONCHART; kk++)
	{
		m_vBarChart[kk] = 0L;
	}

	// scan the map, update items in columns alike
	// new range is set every cycle using the max value found
	//
	POSITION	pos			= m_mFrequencies.GetStartPosition();
	size_t		uInterval	= (size_t)ceil((float)m_stat_max / (float)MAXBARSONCHART);
	size_t		uIndex;
	CStatData	cData;
	long		lKey;

	while (NULL != pos)
	{
		// lKey and cData.Reference() will have the same value
		//
		m_mFrequencies.GetNextAssoc(pos, lKey, cData);

		// find the right column and update value
		//
		uIndex = min(MAXBARSONCHART - 1, (size_t)((float)lKey / (float)uInterval));
		m_vBarChart[uIndex] += cData.Frequency();
	}
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
