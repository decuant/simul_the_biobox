// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#pragma once

struct stCharSize
{
	int iLineHeigth ;
	int iCharWidth ;

	stCharSize( void )
	{
		iLineHeigth = 0 ;
		iCharWidth  = 0 ;
	}
} ;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

void     SelectPointFont( CDC * inDc, TCHAR * inFacename, int inHeight ) ;
void     SelectThisFont( CDC * inDc, TCHAR * inFacename, int inHeight, bool inBold = false, int inDegrees = 0 ) ;
CFont *  SelectThisFont( CWnd * inWindow, TCHAR * inFacename, int inHeight, bool inBold = false, int inDegrees = 0 ) ;
void     CalcCharSize( CDC * inDc, stCharSize & ouCalc ) ;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
