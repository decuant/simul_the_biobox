// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// DlgBioTelSettings.h : header file
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "FakeReader.h"
#include "FakeReaderDlg.h"

// ----------------------------------------------------------------------------
// struct stBioTelSettings
// ----------------------------------------------------------------------------

struct stBioTelSettings
{
	CString m_sParam0;
	CString m_sParam1;
	CString m_sParam2;

	stBioTelSettings()
	{

	}
};

// ----------------------------------------------------------------------------
// class DlgBioTelSettings 
// ----------------------------------------------------------------------------

class CDlgBioTelSettings : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgBioTelSettings)

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_BIOTEL };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	virtual BOOL OnInitDialog(void);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);

	afx_msg void OnEnUpdateEdparam1();
	afx_msg void OnEnUpdateEdparam2();
	afx_msg void OnEnUpdateEdparam3();

	DECLARE_MESSAGE_MAP()

private:
	// members
	//
	CBrush				m_DlgBrushBack;			// brush for the background

	stBioTelSettings	m_Settings;						// original settings read from the plotter
	bool				m_bInitDialog;

	CFakeReaderDlg		* m_DlgReader;
	CEdit				m_EdParam0;
	CEdit				m_EdParam1;
	CEdit				m_EdParam2;

public:

	CDlgBioTelSettings(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgBioTelSettings();

	void AttachMainDialog(CFakeReaderDlg * inDialog);

};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
