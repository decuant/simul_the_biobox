// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// FakeReaderDlg.h : header file
//

#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "PlotView.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// constants for the Simulator running
//
#define	IDT_TIMER_PUMP			(WM_USER + 200)
#define TIMER_INTERVAL_BIOBOX	80
#define TIMER_INTERVAL_FAST		15
#define FRAMESIZE				800
#define MAXRECVMSG				64
#define BIND_TO_PORT			11311

#define STARTOFPACKET			0X42

// constants for the graphics controls
//
#define IDT_TIMER_CTRLS			(IDT_TIMER_PUMP + 100)
#define TIMER_INTERVAL_CTRLS	5000

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// CFakeReaderDlg dialog
class CFakeReaderDlg : public CDialogEx
{
// Construction
public:
	CFakeReaderDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FAKEREADER_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

protected:

	HICON			m_hIcon;
	CBrush			m_DlgBrushBack;			// brush for the background
	CFont			m_FontLogging;			// pretty align the output
	CProgressCtrl	m_Progress;
	CEdit			m_EdLogging;
	CEdit			m_EdLocalPort;
	CListBox		m_LsRecentFiles;
	CStatic			m_PlotCtrl;
	CStatic			m_txTimesAt;			// show time reference for the frame

	CButton			m_ChkCyclic;
	CButton			m_ChkLiveFeed;
	CStatic			m_FileProperties;

	CPlotView		m_Plotter;				// this is the drawing class
	CPoint			* m_Points;				// vector of points that must be drawn
	bool			m_DrawChanged;			// flag a change is necessary
	eDrawFunction	m_eDrawType;			// the type of drawing we want
	USHORT			m_uDrawChannel;			// the channel we want
	int				m_iDrawBiotel;

	CComboBox		m_cbDrawType;			// select drawing function
	CComboBox		m_cbDrawChannel;		// select drawing channel
	CComboBox		m_cbDrawBiotel;
	CEdit			m_EdSeekValue;			// file record index for displaying


public:

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnWindowPosChanged(WINDOWPOS* lpwndpos);

	afx_msg void OnBnClickedStarttimer();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnClose();

	afx_msg void OnBnClickedBtnaddfile();
	afx_msg void OnBnClickedBtndelrecent();
	afx_msg void OnLbnSelchangeLsfilename();

	afx_msg void OnCbnSelchangeCbdrawtype();
	afx_msg void OnCbnSelchangeCbdrawchannel();
	afx_msg void OnCbnSelchangeCbbiotel();

	afx_msg void OnEnUpdateEdSeekValue();
	afx_msg void OnStnDblclickWinplot();

	afx_msg void OnBnClickedBtnSeekFirst();
	afx_msg void OnBnClickedBtnSeekLast();
	afx_msg void OnBnClickedBtnSeekPrev();
	afx_msg void OnBnClickedBtnSeekNext();

	DECLARE_MESSAGE_MAP()
	
	// ----------------------------------------------------------------------------
	// this is the main part of our simulator	
	// ----------------------------------------------------------------------------

private:

	// BioBox protocol's messages
	//
	enum eProtocolState
	{
		eProtUndefinedState		= 0,
		eProtConnectToVest		= 1,
		eProtDisconnectFromVest	= 2,
		eProtGetDeviceState		= 3,
		eProtStartCapture		= 6,
		eProtStopCapture		= 7,
		eProtGetLiveData		= 8,
		eProtUnsolicited		= 9
	};

	HANDLE			m_hInputFile;			// handle for the input file
	int				m_iFileSize;			// file's size
	int				m_iLoop;				// current loop number (associated with the timer)
	int				m_iChunks;				// total number of chunks we are going to read from file
	bool			m_bTimerActive;			// the timer is active or not

	USHORT			m_uLocalPort;			// the port we are binding to
	USHORT			m_uTimerSpeed;			// timer frequency
	CAsyncSocket	m_TcpListener;			// socket for accepting a client
	CAsyncSocket	m_TcpConnection;		// socket for the data communication
	eProtocolState	m_eProtRequest;			// protocol request from host
	eProtocolState	m_eCurProtState;		// protocol's step

	unsigned char	m_FrameBuff[FRAMESIZE];	// data read from file then sent via socket
	unsigned char	m_RecvBuff[MAXRECVMSG];
	int				m_iRecvLen;

	// additional stuff for browsing file
	//
	int  			m_iSeekChunk;			// multiple times 800
	unsigned char	m_FileBuff[FRAMESIZE];	// data read from file then sent via socket

	bool			__reentry;

protected:

	// file management
	//
	void	ResetAcquisition(void);
	bool	OpenDataFile(void);
	bool	CloseDataFile(void);
	bool	ReadFrameFromFile(bool inCiclic, bool inLiveFeed);

	// communication management
	//
	bool	ConnectSocket(void);
	bool	DisconnectSocket(void);
	bool	ParseProtocolMsg(void);
	bool	SendFrameToHost(void);

	// logging facility
	//
	void	ClearLogText(void);
	void	AppendLogText(LPCWSTR inText);
	void	TraceBuffer(const BYTE * inBuffer, int inBufLen);

	// recent file list management
	//
	void	SaveRecentFilesList(void);
	void	LoadRecentFilesList(void);

	// application
	//
	bool	LoadSettings(void);
	bool	SaveSettings(void);
	bool	GetFrameUpdate(void);
	void	InitPlotter(void);
	bool	ReadFileChunk(void);
	bool	ShowFileChunk(void);
	DWORD	GetSeekPosFromEdit(void);
	void	UpdateTimesAt(size_t inCounter);

	bool	SetNewPlotData(void);
	void	ShowGraphicsCtrls(void);
	void	HideGraphicsCtrls(void);
	void	AlignGraphicsCtrls(void);

	bool	LoadBioTelemetry(void);
	void	UnloadBioTelemetry(void);
	void	ConfigBioTelemetry(void);
	bool	VectorFromText(const CString &inText, float outVector[], int inMaxBound);
	void	ShowBioTelCtrls(void);
	void	AlignBioTelCtrls(void);

protected:

	typedef bool (WINAPIV *pfnRunnerInit) ();
	typedef bool (WINAPIV *pfnRunnerRun) (const USHORT[], float[], float[], size_t);
	typedef bool (WINAPIV *pfnRunnerParams) (int, const float[]);

	pfnRunnerParams	extfx_Runner_Params;
	pfnRunnerInit	extfx_InitRunner;
	pfnRunnerRun	extfx_Run_Runner;

	HMODULE			m_hBioTelLib;					// used witg LoadLibrary
	CString			m_sBioTelParams_0;				// string with parameters
	CString			m_sBioTelParams_1;				// "	"	"	"	"
	CString			m_sBioTelParams_2;				// "	"	"	"	"

public:

	inline const CString & BioTelParam_0(void) const { return m_sBioTelParams_0; }
	inline const CString & BioTelParam_1(void) const { return m_sBioTelParams_1; }
	inline const CString & BioTelParam_2(void) const { return m_sBioTelParams_2; }

	void SetBioTelParam_0(const CString & inString);
	void SetBioTelParam_1(const CString & inString);
	void SetBioTelParam_2(const CString & inString);
};

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
