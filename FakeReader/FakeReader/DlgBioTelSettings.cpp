// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// DlgBioTelSettings.cpp : implementation file
//

#include "stdafx.h"
#include "afxdialogex.h"
#include "FakeReader.h"
#include "DlgBioTelSettings.h"
#include "C_Colors.h"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// DlgBioTelSettings dialog

IMPLEMENT_DYNAMIC(CDlgBioTelSettings, CDialogEx)

CDlgBioTelSettings::CDlgBioTelSettings(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DLG_BIOTEL, pParent)
{
	m_DlgReader		= NULL;
	m_bInitDialog	= true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CDlgBioTelSettings::~CDlgBioTelSettings()
{
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgBioTelSettings::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDPARAM1, m_EdParam0);
	DDX_Control(pDX, IDC_EDPARAM2, m_EdParam1);
	DDX_Control(pDX, IDC_EDPARAM3, m_EdParam2);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CDlgBioTelSettings, CDialogEx)
	ON_WM_CTLCOLOR()
	ON_EN_UPDATE(IDC_EDPARAM1, &CDlgBioTelSettings::OnEnUpdateEdparam1)
	ON_EN_UPDATE(IDC_EDPARAM2, &CDlgBioTelSettings::OnEnUpdateEdparam2)
	ON_EN_UPDATE(IDC_EDPARAM3, &CDlgBioTelSettings::OnEnUpdateEdparam3)
END_MESSAGE_MAP()

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

BOOL CDlgBioTelSettings::OnInitDialog(void)
{
	CDialog::OnInitDialog();

	//	misc
	//
	m_DlgBrushBack.CreateSolidBrush(PapayaWhip_COLOR);

	return TRUE;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

HBRUSH CDlgBioTelSettings::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (NULL != pDC)
	{
		switch (nCtlColor)
		{
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(DarkGreen_COLOR);
			break;

		case CTLCOLOR_EDIT:
			pDC->SetTextColor(Red_COLOR);
			break;

		case CTLCOLOR_DLG:
		case CTLCOLOR_LISTBOX:
			pDC->SetTextColor(DarkGreen_COLOR);
			break;

		case CTLCOLOR_BTN:
			pDC->SetTextColor(Black_COLOR);
			break;
		}

		pDC->SetBkMode(TRANSPARENT);
	}


	return m_DlgBrushBack;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgBioTelSettings::AttachMainDialog(CFakeReaderDlg * inDialog)
{
	m_DlgReader = inDialog;
	if (NULL == m_DlgReader)
	{
		return;
	}

	m_bInitDialog = true;

	// read settings and update controls
	//
	m_Settings.m_sParam0 = m_DlgReader->BioTelParam_0();
	m_Settings.m_sParam1 = m_DlgReader->BioTelParam_1();
	m_Settings.m_sParam2 = m_DlgReader->BioTelParam_2();

	// ---------------------------------------
	//
	CString sValue;

	sValue.Format(L"%s", m_Settings.m_sParam0);
	m_EdParam0.SetWindowTextW(sValue);

	sValue.Format(L"%s", m_Settings.m_sParam1);
	m_EdParam1.SetWindowTextW(sValue);

	sValue.Format(L"%s", m_Settings.m_sParam2);
	m_EdParam2.SetWindowTextW(sValue);

	m_bInitDialog = false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgBioTelSettings::OnEnUpdateEdparam1()
{
	if (true == m_bInitDialog || NULL == m_DlgReader)
	{
		return;
	}

	CString		sValue;

	m_EdParam0.GetWindowTextW(sValue);
	m_DlgReader->SetBioTelParam_0(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgBioTelSettings::OnEnUpdateEdparam2()
{
	if (true == m_bInitDialog || NULL == m_DlgReader)
	{
		return;
	}

	CString		sValue;

	m_EdParam1.GetWindowTextW(sValue);
	m_DlgReader->SetBioTelParam_1(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CDlgBioTelSettings::OnEnUpdateEdparam3()
{
	if (true == m_bInitDialog || NULL == m_DlgReader)
	{
		return;
	}

	CString		sValue;

	m_EdParam2.GetWindowTextW(sValue);
	m_DlgReader->SetBioTelParam_2(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
