// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// FakeReaderDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FakeReader.h"
#include "FakeReaderDlg.h"
#include "afxdialogex.h"
#include "C_Colors.h"
#include "Font.h"
#include <stdexcept>

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#define DEF_RECENTS_FNAME  L"RecentFiles.txt"
#define DEF_FILEPROPSTEXT  L"Select a file from the list to display its properties."

#define DEF_CONFIG_FNAME  L"Reader.ini"

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CFakeReaderDlg dialog

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

CFakeReaderDlg::CFakeReaderDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_FAKEREADER_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

	ResetAcquisition( ) ;

	m_uLocalPort	= BIND_TO_PORT;				// the port we are binding to
	m_uTimerSpeed	= TIMER_INTERVAL_BIOBOX;	// timer frequency

	m_Points		= NULL;
	m_DrawChanged	= true;					// start with a redraw
	m_eDrawType		= eDraw_Fx_Segments;	// just a value
	m_uDrawChannel	= 0;					// start with ecg1
	m_iDrawBiotel	= 0;					// BioTel disabled
	m_iSeekChunk	= 0;

	ZeroMemory(m_FileBuff, FRAMESIZE);

	__reentry		= false;

	// --------------------------------
	// BtioTel specific
	//
	m_hBioTelLib		= NULL;
	extfx_Runner_Params = NULL;
	extfx_InitRunner	= NULL;
	extfx_Run_Runner	= NULL;

	m_sBioTelParams_0	= L"10, 25, 50, 4, 0.001, 0.001, 0.1, 0.1, 2";
	m_sBioTelParams_1	= L"20";
	m_sBioTelParams_2	= L"50, 0.04, 0.15, 10, 15, 0.75, 2";
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::ResetAcquisition(void)
{
	m_hInputFile		= INVALID_HANDLE_VALUE;
	m_iFileSize			= 0;
	m_iChunks			= 0;
	m_iLoop				= 0;
	m_eProtRequest		= eProtUndefinedState;
	m_iRecvLen			= 0;
	m_bTimerActive		= false;
	m_eCurProtState		= eProtUndefinedState;

	ZeroMemory(m_FrameBuff, FRAMESIZE * sizeof(UCHAR));
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//	DDX_Text(pDX, IDC_EDFILENAME, m_EdFilename);
	DDX_Control(pDX, IDC_PROGRESS1, m_Progress);
	DDX_Control(pDX, IDC_EDLOGGING, m_EdLogging);
	DDX_Control(pDX, IDC_EDLOCALPORT, m_EdLocalPort);
	DDX_Control(pDX, IDC_LSFILENAME, m_LsRecentFiles);
	DDX_Control(pDX, IDC_CHKCICLIC, m_ChkCyclic);
	DDX_Control(pDX, IDC_TXTFILEPROPS, m_FileProperties);
	DDX_Control(pDX, IDC_CHKLIVEDATA, m_ChkLiveFeed);
	DDX_Control(pDX, IDC_WINPLOT, m_PlotCtrl);
	DDX_Control(pDX, IDC_CBDRAWTYPE, m_cbDrawType);
	DDX_Control(pDX, IDC_CBDRAWCHANNEL, m_cbDrawChannel);
	DDX_Control(pDX, IDC_ED_SEEK_VALUE, m_EdSeekValue);
	DDX_Control(pDX, IDC_CBBIOTEL, m_cbDrawBiotel);
	DDX_Control(pDX, IDC_LBLTIMESAT, m_txTimesAt);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

BEGIN_MESSAGE_MAP(CFakeReaderDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_CTLCOLOR()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_WINDOWPOSCHANGED()
	ON_WM_TIMER()
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BTNADDFILE, &CFakeReaderDlg::OnBnClickedBtnaddfile)
	ON_BN_CLICKED(IDSTARTTIMER, &CFakeReaderDlg::OnBnClickedStarttimer)
	ON_LBN_SELCHANGE(IDC_LSFILENAME, &CFakeReaderDlg::OnLbnSelchangeLsfilename)
	ON_BN_CLICKED(IDC_BTNDELRECENT, &CFakeReaderDlg::OnBnClickedBtndelrecent)
	ON_CBN_SELCHANGE(IDC_CBDRAWTYPE, &CFakeReaderDlg::OnCbnSelchangeCbdrawtype)
	ON_CBN_SELCHANGE(IDC_CBDRAWCHANNEL, &CFakeReaderDlg::OnCbnSelchangeCbdrawchannel)
	ON_EN_UPDATE(IDC_ED_SEEK_VALUE, &CFakeReaderDlg::OnEnUpdateEdSeekValue)
	ON_BN_CLICKED(IDC_BTN_SEEK_FIRST, &CFakeReaderDlg::OnBnClickedBtnSeekFirst)
	ON_BN_CLICKED(IDC_BTN_SEEK_LAST, &CFakeReaderDlg::OnBnClickedBtnSeekLast)
	ON_BN_CLICKED(IDC_BTN_SEEK_PREV, &CFakeReaderDlg::OnBnClickedBtnSeekPrev)
	ON_BN_CLICKED(IDC_BTN_SEEK_NEXT, &CFakeReaderDlg::OnBnClickedBtnSeekNext)
	ON_STN_DBLCLK(IDC_WINPLOT, &CFakeReaderDlg::OnStnDblclickWinplot)
	ON_CBN_SELCHANGE(IDC_CBBIOTEL, &CFakeReaderDlg::OnCbnSelchangeCbbiotel)
END_MESSAGE_MAP()

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// CFakeReaderDlg message handlers

BOOL CFakeReaderDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		// let edit the settings file with the preferred editor
		// 
		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_STARTTIMING, L"Record Start Timing");
		pSysMenu->AppendMenu(MF_STRING, IDM_STOPTIMING,  L"Record Stop  Timing");

		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_EDITPARAMETERS, L"Edit BioTel Parameters");

		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_EDITSETTINGS, L"Edit Settings");

		pSysMenu->AppendMenu(MF_SEPARATOR);
		pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, L"About the simulator");
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	//	misc
	//
	m_DlgBrushBack.CreateSolidBrush(LightSkyBlue_COLOR);

	// create a specific font "Courier New" to have
	// text aligned properly
	//
	SelectThisFont(& m_EdLogging, L"Courier New", 21, false);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	// show the settings file in an external editor
	//
	if ((nID & 0xFFF0) == IDM_STARTTIMING)
	{
		m_Plotter.SetStartTime();
		if (0 != m_iDrawBiotel)
		{
			ConfigBioTelemetry();
		}

		m_DrawChanged = true;
		PostMessage(WM_PAINT);
	}

	// stop timing
	//
	else if ((nID & 0xFFF0) == IDM_STOPTIMING)
	{
		m_Plotter.SetStopTime();
	}

	// show the settings file in an external editor
	//
	else if ((nID & 0xFFF0) == IDM_EDITSETTINGS)
	{
		::ShellExecute(::GetDesktopWindow(), L"open", L"Reader.ini", NULL, NULL, SW_SHOWDEFAULT);
	}

	// parameters for the BioTel library
	//
	else if ((nID & 0xFFF0) == IDM_EDITPARAMETERS)
	{
		ShowBioTelCtrls();
	}

	// show the about box
	//
	else if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}

	// default processing
	//
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

HBRUSH CFakeReaderDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	if (NULL != pDC)
	{
		switch (nCtlColor)
		{
		case CTLCOLOR_STATIC:
			pDC->SetTextColor(PureDarkRed_COLOR);
			break;
			
		case CTLCOLOR_EDIT:
			pDC->SetTextColor(Black_COLOR);
			break;

		case CTLCOLOR_DLG:
		case CTLCOLOR_LISTBOX:						
			pDC->SetTextColor(DarkGreen_COLOR);
			pDC->SetBkColor(Grey32_COLOR);
			break;

		case CTLCOLOR_BTN:
			pDC->SetTextColor(Black_COLOR);
			break;
		}

		pDC->SetBkMode(TRANSPARENT);
	}

	return m_DlgBrushBack;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnCbnSelchangeCbdrawtype()
{
	m_eDrawType = (eDrawFunction)m_cbDrawType.GetCurSel();

	if (eDrawFunction::eDraw_Fx_Hitmap == m_eDrawType)
	{
		m_Plotter.ForceRefresh();
	}

	m_Plotter.SetDrawingFunction(m_eDrawType);

	m_DrawChanged = true;
	PostMessage(WM_PAINT);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnCbnSelchangeCbdrawchannel()
{
	m_uDrawChannel = (USHORT)m_cbDrawChannel.GetCurSel();

	if (false == m_bTimerActive)
	{
		ShowFileChunk();
	}

	m_DrawChanged = true;
	PostMessage(WM_PAINT);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnCbnSelchangeCbbiotel()
{
	int iValueSelected  = m_cbDrawBiotel.GetCurSel();

	if (0 == iValueSelected)
	{
		// check if we were using the library
		//
		if (NULL != m_hBioTelLib)
		{
			UnloadBioTelemetry();
			AppendLogText(L"[BioTel.dll] Library unloaded");
		}

		goto _loadbiotel;
	}

	// check that the library is loaded correctly
	// otherwise change the value back to disabled
	//
	{
		if (0 == m_iDrawBiotel)
		{
			if (false == LoadBioTelemetry())
			{
				iValueSelected = 0;
				m_cbDrawBiotel.SetCurSel(iValueSelected);
				AppendLogText(L"BioTel channel selection cancelled");

				goto _loadbiotel;
			}

			// carry on
			//
			ConfigBioTelemetry();
		}
	}

_loadbiotel:

	// exit check the valid value we have
	//
	m_iDrawBiotel = iValueSelected;

	// ask for a redraw
	//
	m_DrawChanged = true;
	PostMessage(WM_PAINT);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CFakeReaderDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);

		m_DrawChanged = true;
		return;
	}

	// draw the plot here
	//
	CDC * dc = m_PlotCtrl.GetDC();

	m_Plotter.Draw(dc);
	ReleaseDC(dc);

	// normal dialog processing
	//
	CDialog::OnPaint();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CFakeReaderDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CWaitCursor wCursor;

	CDialogEx::OnShowWindow(bShow, nStatus);

	//--------------------------------------------
	// default data filename
	//
	m_LsRecentFiles.ResetContent();
	LoadRecentFilesList();
	m_FileProperties.SetWindowTextW(DEF_FILEPROPSTEXT);

	// ----------------------------------------
	// load settings
	//
	LoadSettings();

	{
		CString sValue;

		sValue.Format(L"%d", m_uLocalPort);
		m_EdLocalPort.SetWindowTextW(sValue);
	}

	//--------------------------------------------
	// setup the socket
	//
	AfxSocketInit();
	m_TcpListener.Create(m_uLocalPort);

	// file viewer
	//
	m_EdSeekValue.SetWindowTextW(L"0");

	//--------------------------------------------
	// setup the drawing control
	//
	InitPlotter();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::InitPlotter(void)
{
	CRect   rcClient;
	float	fOldScaleX = m_Plotter.ScaleX();			// these values may be valid
	float	fOldScaleY = m_Plotter.ScaleY();			// because read from settings

	m_PlotCtrl.GetClientRect(&rcClient);
	m_Plotter.SetWindowRect(rcClient);

	// allocate memory
	//
	m_Points = new CPoint[DRAW_MAX_SAMPLES];

	m_Plotter.InitializeMemory(DRAW_MAX_SAMPLES, 1);	// 80 is our reference time

	// check for the drawing function
	//
	m_cbDrawChannel.SetCurSel(m_uDrawChannel);				// select ECG 1, 2, 3, 4, 5
	m_cbDrawBiotel.SetCurSel(m_iDrawBiotel);
	m_cbDrawType.SetCurSel(m_eDrawType);

	m_Plotter.SetDrawingFunction(m_eDrawType);

	// check if we have values read from the settings file...
	//
	if (0.0f != fOldScaleX)
	{
		m_Plotter.OverrideScale(fOldScaleX, m_Plotter.ScaleY());
	}

	if (0.0f != fOldScaleY)
	{
		m_Plotter.OverrideScale(m_Plotter.ScaleX(), fOldScaleY);
	}

	// ask for a redraw
	//
	m_DrawChanged = true;
	PostMessage(WM_PAINT);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtnaddfile()
{
	// remember where we are now
	//
	TCHAR szDirectory[MAX_PATH+1] = { 0 };

	_wgetcwd(szDirectory, MAX_PATH);

	// prompt the user
	//
	CFileDialog    FileDlg(	true, L"BIN", NULL,
							OFN_HIDEREADONLY,
							L"Binary files (*.bin)|*.bin|All Files (*.*)|*.*||",
							NULL);

	CString           sFilename;

	// a file was selected
	// we don't care about the type
	//
	if (IDCANCEL != FileDlg.DoModal())
	{
		sFilename = FileDlg.GetPathName();
		m_LsRecentFiles.InsertString(0, sFilename);		// last first

		m_LsRecentFiles.SetCurSel(0);
		OnLbnSelchangeLsfilename();
	}

	// restore original directory
	//
	_wchdir(szDirectory);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtndelrecent()
{
	if (-1 < m_LsRecentFiles.GetCurSel())
	{
		m_LsRecentFiles.DeleteString(m_LsRecentFiles.GetCurSel());
		m_FileProperties.SetWindowTextW(DEF_FILEPROPSTEXT);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedStarttimer()
{
	// start the timer
	//
	if (false == m_bTimerActive)
	{
		// restart clean
		//
		ResetAcquisition();
		ClearLogText();

		// restart the listener
		//
		m_TcpListener.Close();

		CString sTemp;

		m_EdLocalPort.GetWindowTextW(sTemp);
		m_uLocalPort = (USHORT)_wtol(sTemp);

		if (80 > m_uLocalPort || 65535 <= m_uLocalPort)
		{
			m_uLocalPort = BIND_TO_PORT;
		}

		if (TRUE == m_TcpListener.Create(m_uLocalPort))
		{
			CString sMessage;

			sMessage.Format(L"Using local port [%d]", m_uLocalPort);
			AppendLogText(sMessage);
		}

		//	try open the selected file
		//
		if (false == OpenDataFile())
		{
			AppendLogText(L"Unable to open the specified file");
			return;
		}
		AppendLogText(L"File open OK");

		// setup the dialog's timer pump
		//
		if (0 == SetTimer(IDT_TIMER_PUMP, m_uTimerSpeed, NULL))
		{
			AppendLogText(L"Unable to start the timer");
			CloseDataFile();
			return;
		}
		m_bTimerActive = true;
		AppendLogText(L"Timer started");

		// ----------------------
		// reset the plotter
		//
		m_Plotter.SetStartTime();

		m_DrawChanged = true;
		PostMessage(WM_PAINT);

		// reset the seek position in the entry field
		//
		m_EdSeekValue.SetWindowTextW(L"0");
		m_iSeekChunk = 0;

		return;
	}

	// check if the user issued a stop
	// or we have reached the end of file
	//
	if(true == m_bTimerActive || m_iChunks == m_iLoop)
	{
		KillTimer(IDT_TIMER_PUMP);
		DisconnectSocket();

		if (true == m_bTimerActive)
		{
			m_TcpListener.Close();
		}

		CloseDataFile();
		ResetAcquisition();

		// need to refresh the drawing after the stop
		//
		m_Plotter.SetStopTime();

		m_DrawChanged = true;
		PostMessage(WM_PAINT);

		AppendLogText(L"Timer deactivated");
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::OpenDataFile()
{
	if (INVALID_HANDLE_VALUE != m_hInputFile)
	{
		AppendLogText(L"Rolling back to start of file");

		SetFilePointer(m_hInputFile, 0, NULL, SEEK_CUR);
		m_Progress.SetPos(0);
		m_iLoop = 0;

		// update the stop time for the new plot data
		//
		m_Plotter.SetStartTime();

		return true;
	}

	// check we have a valid selection
	//
	if (0 > m_LsRecentFiles.GetCurSel())
	{
		return false;
	}

	// need to update the filename in the text field
	//
	CString sFilename;
	m_LsRecentFiles.GetText(m_LsRecentFiles.GetCurSel(), sFilename);

	if (true == sFilename.IsEmpty())
	{
		return false;
	}

	m_hInputFile = CreateFileW(sFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE != m_hInputFile)
	{
		// setup the progress control
		//
		m_iFileSize = GetFileSize(m_hInputFile, NULL);
		m_iChunks	= m_iFileSize / FRAMESIZE;
		m_iLoop		= 0;

		m_Progress.SetRange(0, m_iChunks);
		m_Progress.SetPos(0);

		// update the start time for the new plot data
		//
		m_Plotter.SetStartTime();

		return true;
	}

	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::CloseDataFile(void)
{
	if (INVALID_HANDLE_VALUE != m_hInputFile)
	{		
		CloseHandle((HANDLE)m_hInputFile);

		// update the stop time for the new plot data
		//
		m_Plotter.SetStopTime();

		CString sText;

		sText.Format(L"Number of read cycles [%d] of [%d]", m_iLoop, m_iChunks);
		AppendLogText(sText);
	}

	m_hInputFile = INVALID_HANDLE_VALUE;
	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::ReadFrameFromFile(bool inCiclic, bool inLiveFeed)
{
	if (INVALID_HANDLE_VALUE == m_hInputFile)
		return false;

	// initialize always
	//
	ZeroMemory(m_FrameBuff, FRAMESIZE * sizeof(UCHAR));

	// check if we are real time streaming
	// may not have all the bytes available
	//
	if (true == inLiveFeed)
	{
		// get the number of bytes available from current position on
		//
		DWORD dwCurPos = SetFilePointer(m_hInputFile, 0, NULL, SEEK_CUR);
		DWORD dwEndPos = SetFilePointer(m_hInputFile, 0, NULL, SEEK_END);

		// restore position
		//
		SetFilePointer(m_hInputFile, dwCurPos, NULL, SEEK_SET);

		if (true == (FRAMESIZE > (dwEndPos - dwCurPos)))
		{
			// this is an underrun error
			// we will fake a successfull read and send back
			// a frame full of zeros
			//
			AppendLogText(L"WARNING: data not yet available, cycle lost");
			return true;
		}

		// new data has been arrived, adjust values
		//
		if (dwEndPos != m_iFileSize)
		{
			m_iFileSize = dwEndPos;
			m_iChunks   = m_iFileSize / FRAMESIZE;
			m_Progress.SetRange(0, m_iChunks);
		}
	}

	// perform the read file
	//
	DWORD dwBytesRead = 0;

	ReadFile(m_hInputFile, m_FrameBuff, FRAMESIZE, & dwBytesRead, NULL);

	if (FRAMESIZE > dwBytesRead)
	{
		// log an error and stop the whole lot
		//
		AppendLogText(L"ERROR: data trucated on the file");

		// stop sending if no more data available
		//
		CloseDataFile();

		if (true == inCiclic)
		{
			OpenDataFile();
			m_iLoop = -1;
		}
	}

	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::ConnectSocket(void)
{
	if (TRUE == m_TcpListener.Listen(0))
	{
		if (INVALID_SOCKET != m_TcpConnection.m_hSocket)
		{
			m_TcpConnection.Close();
			m_TcpConnection.m_hSocket = INVALID_SOCKET;
		}

		if (TRUE == m_TcpListener.Accept(m_TcpConnection))
		{
			AppendLogText(L"Socket is correctly connected");
			return true;
		}
	}

	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::DisconnectSocket(void)
{
	if (INVALID_SOCKET != m_TcpConnection.m_hSocket)
	{
		m_TcpConnection.ShutDown(SD_BOTH);
		m_TcpConnection.Close();
		m_TcpConnection.m_hSocket = INVALID_SOCKET;

		AppendLogText(L"Connection closed.");

		m_eCurProtState	= eProtUndefinedState;
		m_eProtRequest	= eProtUndefinedState;
	}

	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::SendFrameToHost(void)
{
	unsigned char	uData[FRAMESIZE + 20];
	int			iToSend = 0;
	int			iSentOk = 0;

	// check which protocol message we are going to answer
	//
	switch (m_eProtRequest)
	{
		case eProtConnectToVest:
		{
			uData[0] = STARTOFPACKET;
			uData[1] = eProtConnectToVest;
			uData[2] = 0x00;

			iToSend = 3;
			iSentOk = m_TcpConnection.Send(uData, iToSend);
			m_eCurProtState = eProtConnectToVest;

			AppendLogText(L"Reply to ConnectToVest");

			// this is necessary when we previously received a stop command,
			// this allow us to keep the progress bar showing how much data
			// we have sent so far
			//
			OpenDataFile();
		}
		break;

		case eProtDisconnectFromVest:
		{
			uData[0] = STARTOFPACKET;
			uData[1] = eProtDisconnectFromVest;
			uData[2] = 0x00;

			iToSend = 3;
			iSentOk = m_TcpConnection.Send(uData, iToSend);
			m_eCurProtState = eProtDisconnectFromVest;

			AppendLogText(L"Reply to DisconnectFromVest");

			// disconnect the socket
			//
			DisconnectSocket();
			CloseDataFile();
		}
		break;

		case eProtGetDeviceState:
		{
			uData[0] = STARTOFPACKET;
			uData[1] = eProtGetDeviceState;
			uData[2] = 0x00;
			uData[3] = 0x02;				// device capturing

			iToSend = 4;
			iSentOk = m_TcpConnection.Send(uData, iToSend);
			m_eCurProtState = eProtGetDeviceState;

			AppendLogText(L"Reply to GetDeviceState");
		}
		break;

		case eProtStartCapture:
		{
			uData[0] = STARTOFPACKET;
			uData[1] = eProtStartCapture;
			uData[2] = 0x00;

			iToSend = 3;
			iSentOk = m_TcpConnection.Send(uData, iToSend);
			m_eCurProtState = eProtStartCapture;

			AppendLogText(L"Reply to StartCapture");
		}
		break;

		case eProtStopCapture:
		{
			uData[0] = STARTOFPACKET;
			uData[1] = eProtStopCapture;
			uData[2] = 0x00;

			iToSend = 3;
			iSentOk = m_TcpConnection.Send(uData, iToSend);
			m_eCurProtState = eProtStopCapture;

			AppendLogText(L"Reply to StopCapture");

			// restart the file reader index
			//
			CloseDataFile();

			// this should not be here, it's only a stop not a disconnect!!!
			// (the stop from WTSServer will break the connection)
			//
			DisconnectSocket();
		}
		break;

		case eProtGetLiveData:
		case eProtUnsolicited:
		{
			if (eProtStopCapture == m_eCurProtState)
			{
				break;
			}

			// this is an unsolicited message
			//
			if (true == GetFrameUpdate())
			{
				uData[0] = STARTOFPACKET;
				uData[1] = eProtGetLiveData;
				uData[2] = 0x01;						// Antonio: I let a drift of 1/10th
				uData[3] = 0x00;
				uData[4] = 0x00;
				uData[5] = 0x00;

				iToSend = 6 + FRAMESIZE;
				memcpy(uData + 6, m_FrameBuff, FRAMESIZE);
				iSentOk = m_TcpConnection.Send(uData, iToSend);
				m_eCurProtState = eProtGetLiveData;

				if (eProtUnsolicited != m_eProtRequest)
				{
					AppendLogText(L"Reply to GetLiveData");
				}
			}
		}
		break;

		default:
		{
			CString sText;

			sText.Format(L"Unknown start packet [%d] received", m_eProtRequest);
			AppendLogText(sText.GetString());

			m_eProtRequest = eProtUndefinedState;
			return false;
		}
	}

	// check if all bytes were sent ok
	//
	if (iToSend != iSentOk)
	{
		// tbd we should retry
		//
		CString sText;

		sText.Format(L"Failed to send: Tot/OK [%2d/%2d]", iToSend, iSentOk);
		AppendLogText(sText);

		// treat this error as a socket down
		//
		if (-1 == iSentOk)
		{
			AppendLogText(L"Aborting connection.");

			DisconnectSocket();
			m_eProtRequest = eProtUndefinedState;
		}

		return false;
	}

	m_eProtRequest = eProtUndefinedState;
	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::ParseProtocolMsg(void)
{
	// hide a lot of messages
	//
	if (eProtGetLiveData != m_eCurProtState)
	{
		CString sText;

		sText.Format(L"Bytes received total [%d]", m_iRecvLen);
		AppendLogText(sText.GetString());
	}

	if (STARTOFPACKET == m_RecvBuff[0])
	{
		m_eProtRequest = (eProtocolState)m_RecvBuff[1];

		// this is just logging for debugging the protocol on site
		//
		switch (m_eProtRequest)
		{
		case eProtUndefinedState:
		default:
			AppendLogText(L"--> Prot. Undefined State");
			break;

		case eProtConnectToVest:
			AppendLogText(L"--> Prot. Connect To Vest");
			break;

		case eProtDisconnectFromVest:
			AppendLogText(L"--> Prot. Disconnect From Vest");
			break;

		case eProtGetDeviceState:
			AppendLogText(L"--> Prot. Get Device State");
			break;

		case eProtStartCapture:
			AppendLogText(L"--> Prot. Start Capture");
			break;

		case eProtStopCapture:
			AppendLogText(L"--> Prot. Stop Capture");
			break;

		case eProtGetLiveData:
			AppendLogText(L"--> Prot. Get Live Data");
			break;
		}

		return true;
	}

	{
		CString sText;

		sText.Format(L"ERROR: Unknwon protocol msg [%02X] received, dump follows:", m_RecvBuff[0]);
		AppendLogText(sText.GetString());

		TraceBuffer(m_RecvBuff, m_iRecvLen); 
	}

	// flag the error
	//
	m_eProtRequest = eProtUndefinedState;
	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::GetFrameUpdate(void)
{
	if (true == ReadFrameFromFile(0 != m_ChkCyclic.GetCheck(), 0 != m_ChkLiveFeed.GetCheck()))
	{
		m_Progress.SetPos(m_iLoop++);

		CString sValue;

		__reentry = true;
		sValue.Format(L"%d", m_iLoop);
		m_EdSeekValue.SetWindowTextW(sValue);
		__reentry = false;

		// profit of the chance for updating our drawing
		//
		if (eDraw_Fx_Disabled != m_Plotter.DrawFunction())
		{
			SetNewPlotData();
		}

		return true;
	}

	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::SetNewPlotData(void)
{
	// check if we have a channel from BioTel to look at
	//
	if (0 == m_iDrawBiotel)
	{
		// standard processing 
		//
		for (int kk = 0; kk < DRAW_MAX_SAMPLES; kk++)
		{
			USHORT uOffset;

			uOffset = m_uDrawChannel * 2 + kk * 10;

			m_Points[kk].x = kk * 1;
			m_Points[kk].y = m_FrameBuff[uOffset] | (m_FrameBuff[uOffset + 1] << 8);
		}

		m_Plotter.UpdateData(m_Points, DRAW_MAX_SAMPLES);
	}
	else
	{
		const int iTempVectLen = 80;			// make copies for BioTel calculation
		USHORT vTemp_ECG[iTempVectLen];
		float  vTemp_LFHF[iTempVectLen];
		float  vTemp_HR[iTempVectLen];

		// BioTel preprocessing
		//
		for (int kk = 0; kk < DRAW_MAX_SAMPLES; kk++)
		{
			USHORT uOffset;

			uOffset = m_uDrawChannel * 2 + kk * 10;
			vTemp_ECG[kk] = m_FrameBuff[uOffset] | (m_FrameBuff[uOffset + 1] << 8);

			// init memory
			//
			vTemp_LFHF[kk] =
			vTemp_HR[kk] = 0.0f;
		}

		// run the runner
		//
		bool bError = false;

		try
		{
			if (false == ((*extfx_Run_Runner) (vTemp_ECG, vTemp_HR, vTemp_LFHF, iTempVectLen)))
			{
				AppendLogText(L"[BioTel.dll] Runner failed.");
			}
		}
		catch (std::invalid_argument)
		{
			bError = true;
			AppendLogText(L"[BioTel.dll] Runner invalid argument.");
		}
		catch (std::logic_error)
		{
			bError = true;
			AppendLogText(L"[BioTel.dll] Runner logic error.");
		}

		if (false == bError)
		{
			// copy over
			//
			for (int kk = 0; kk < DRAW_MAX_SAMPLES; kk++)
			{
				m_Points[kk].x = kk * 1;

				switch (m_iDrawBiotel)
				{
				case 1:									// HR

					m_Points[kk].y = (USHORT)vTemp_HR[kk];
					break;

				case 2:									// LF/HF

					m_Points[kk].y = (USHORT)(vTemp_LFHF[kk] * 1000.0f);
					break;
				}
			}

			m_Plotter.UpdateData(m_Points, DRAW_MAX_SAMPLES);
		}
	}

	m_DrawChanged = true;
	PostMessage(WM_PAINT);

	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnTimer(UINT_PTR nIDEvent)
{
	// check for errors on a valid connection
	//
	if (INVALID_SOCKET != m_TcpConnection.m_hSocket && 0 != m_TcpConnection.GetLastError())
	{
		CString sText;

		sText.Format(L"Error on the client socket [%d]", m_TcpConnection.GetLastError());
		AppendLogText(sText.GetString());

		m_TcpConnection.Close();
		m_TcpConnection.m_hSocket = INVALID_SOCKET;

		goto _drawFx;
	}

	//	check if we have a valid connection or try to create one
	//
	if (INVALID_SOCKET == m_TcpConnection.m_hSocket)
	{
		if (false == ConnectSocket())
		{
			goto _drawFx;
		}

		m_eProtRequest  = eProtUndefinedState;
		m_eCurProtState = eProtUndefinedState;
	}

	// check for a message from host
	//
	if (eProtUndefinedState == m_eProtRequest)
	{
		m_iRecvLen		= m_TcpConnection.Receive(m_RecvBuff, MAXRECVMSG);
		m_eProtRequest	= eProtUndefinedState;

		if (0 < m_iRecvLen)
		{
			ParseProtocolMsg();
		}
	}

	if (eProtUndefinedState != m_eProtRequest)
	{
		// answer a question
		//
		SendFrameToHost();
		m_eProtRequest = eProtUndefinedState;
	}
	else if (eProtGetLiveData == m_eCurProtState)
	{
		// we are at the sending step
		//
		m_eProtRequest = eProtUnsolicited;
		SendFrameToHost();
		m_eProtRequest = eProtUndefinedState;
	}

	// ---------------------------------------
	// new part, show the sinc function
	//
_drawFx:

	if (true == m_DrawChanged || m_Plotter.IsModified())
	{
		m_Plotter.Invalidate();
		PostMessage(WM_PAINT);
		m_DrawChanged = false;
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

static int iNumLines = 0;

void CFakeReaderDlg::ClearLogText(void)
{
	if (0 < m_EdLogging.GetWindowTextLengthW())
	{
		m_EdLogging.SetSel(0, -1);
		m_EdLogging.ReplaceSel(L"");

		iNumLines = 0;
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::AppendLogText(LPCWSTR inText)
{
	if (100 < ++iNumLines)
	{
		ClearLogText();
	}

	if (0 < m_EdLogging.GetWindowTextLengthW())
	{
		m_EdLogging.SetSel(m_EdLogging.GetWindowTextLengthW(), m_EdLogging.GetWindowTextLengthW());
		m_EdLogging.ReplaceSel(L"\r\n");
	}

	m_EdLogging.SetSel(m_EdLogging.GetWindowTextLengthW(), m_EdLogging.GetWindowTextLengthW());
	m_EdLogging.ReplaceSel(inText);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// text which goes into the logging edit control
//
void CFakeReaderDlg::TraceBuffer(const BYTE * inBuffer, int inBufLen)
{
	if (0 >= inBufLen)
	{
		return;
	}

	CString szFormat;
	CString szTextLine;

	const BYTE *chunk = inBuffer;

	for (int i = 0; i<inBufLen; i += 16)
	{
		szFormat.Format(_T("%04X: "), i);
		szTextLine += szFormat;

		for (int j = 0; j < 16; j++)
		{
			if (i + j < inBufLen)
			{
				szFormat.Format(_T("%02x "), chunk[i + j]);
			}
			else
			{
				szFormat = _T("   ");
			}

			szTextLine += szFormat;
		}
		szTextLine += _T(' ');

		for (int j = 0; j < 16; j++)
		{
			if (i + j < inBufLen)
			{
				szFormat.Format(_T("%c"), isprint(chunk[i + j]) ? chunk[i + j] : _T('.'));
				szTextLine += szFormat;
			}
		}

		AppendLogText(szTextLine.GetString());
		szTextLine.Empty();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::SaveRecentFilesList(void)
{
	CStdioFile	hFileList;

	if (TRUE == hFileList.Open(DEF_RECENTS_FNAME, CFile::modeWrite | CFile::modeCreate | CFile::shareExclusive | CFile::typeText))
	{
		CWaitCursor    wait;

		CString sTemp;

		for (int w = 0; w < m_LsRecentFiles.GetCount(); w++)
		{
			m_LsRecentFiles.GetText(w, sTemp);
			sTemp += L"\n";
			hFileList.WriteString(sTemp);
		}

		hFileList.Close();
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::LoadRecentFilesList(void)
{
	CStdioFile	hFileList;

	if (TRUE == hFileList.Open(DEF_RECENTS_FNAME, CFile::modeRead | CFile::shareExclusive | CFile::typeText))
	{
		CWaitCursor    wait;

		CString sTemp;

		while(TRUE == hFileList.ReadString(sTemp))
		{
			sTemp = sTemp.Trim();
			if (false == sTemp.IsEmpty())
			{
				m_LsRecentFiles.AddString(sTemp);
			}
		}

		hFileList.Close();
	}
}


// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnClose()
{
	DisconnectSocket();				// abort everything
	CloseDataFile();

	SaveRecentFilesList();			// save program' status
	SaveSettings();

	CDialogEx::OnClose();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// display file properties when the user changes the selection
//
void CFakeReaderDlg::OnLbnSelchangeLsfilename()
{
	CString			sFilename;
	HANDLE			hInputFile;		// handle for the input file

	m_FileProperties.SetWindowTextW(DEF_FILEPROPSTEXT);

	if (0 > m_LsRecentFiles.GetCurSel())
	{
		return;
	}

	m_LsRecentFiles.GetText(m_LsRecentFiles.GetCurSel(), sFilename);
	if (true == sFilename.IsEmpty())
	{
		return;
	}

	hInputFile = CreateFileW(sFilename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	if (INVALID_HANDLE_VALUE != hInputFile)
	{
		long			iFileSize;			// file's size
		long			iChunks;			// total number of chunks we are going to read from file
		long			iTimeRun;			// the expected execution time
		long			iTimeSim;			// the expected simulation time
		
		iFileSize	= GetFileSize(hInputFile, NULL);
		iChunks		= iFileSize / FRAMESIZE + ((0 < (iFileSize % FRAMESIZE)) ? 1 : 0);		// count the underrun too
		iTimeRun	= (long)ceil((iChunks * TIMER_INTERVAL_BIOBOX) / 1000.0f);
		iTimeSim	= (long)ceil((iChunks * m_uTimerSpeed) / 1000.0f);

		CloseHandle((HANDLE)hInputFile);

		{
			CString			sTxtProps;
			CString			sTxtSize;
			CString			sTxtChunks;
			CString			sTxtTimeRun;
			CString			sTxtTimeSim;

			// print file size
			//
			iFileSize /= 1024;
			if (1024 < iFileSize)
			{
				sTxtSize.Format(L"File Size [%d.%d Mb]", iFileSize / 1024, iFileSize % 1024);
			}
			else
			{
				sTxtSize.Format(L"File Size [%d Kb]", iFileSize);
			}

			// print time run
			//
			if (60 < iTimeRun)
			{
				sTxtTimeRun.Format(L"Run Time [%d.%02d min]", iTimeRun / 60, iTimeRun % 60);
			}
			else
			{
				sTxtTimeRun.Format(L"Run Time [%d sec]", iTimeRun);
			}

			// print time simulation
			//
			if (60 < iTimeSim)
			{
				sTxtTimeSim.Format(L"Sim. Time [circa %d.%02d min @ %d msecs]", iTimeSim / 60, iTimeSim % 60, m_uTimerSpeed);
			}
			else
			{
				sTxtTimeSim.Format(L"Sim. Time [circa %d sec @ %d msecs]", iTimeSim, m_uTimerSpeed);
			}

			// print chunks
			//
			sTxtChunks.Format(L"Chunks [%d]", iChunks);

			sTxtProps  = sTxtSize;
			sTxtProps += L"  ";
			sTxtProps += sTxtChunks;
			sTxtProps += L"  ";
			sTxtProps += sTxtTimeRun;
			sTxtProps += L"  ";
			sTxtProps += sTxtTimeSim;

			m_FileProperties.SetWindowTextW(sTxtProps);
		}

		return;
	}

	m_FileProperties.SetWindowTextW(L"File not found or cannot open it.");
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::ReadFileChunk(void)
{
	CString			sFilename;
	HANDLE			hChunksFile;		// handle for the input file
	bool			b_ret = false;

	if (0> m_LsRecentFiles.GetCurSel())
	{
		m_iSeekChunk = 0;
		return b_ret;
	}

	m_LsRecentFiles.GetText(m_LsRecentFiles.GetCurSel(), sFilename);
	if (true == sFilename.IsEmpty())
	{
		m_iSeekChunk = 0;
		return b_ret;
	}

	hChunksFile = CreateFileW(sFilename, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	// advance to the requested position and do the read
	//
	if (INVALID_HANDLE_VALUE != hChunksFile)
	{
		DWORD dwEndPos	= SetFilePointer(hChunksFile, 0, NULL, SEEK_END);
		DWORD dwCurPos	= SetFilePointer(hChunksFile, 0, NULL, SEEK_SET);
		DWORD dwSeekPos = m_iSeekChunk * FRAMESIZE;

		// seek to the end issued or passed the end of file?
		//
		if (-1 == m_iSeekChunk || dwSeekPos > dwEndPos)
		{
			dwSeekPos = dwEndPos - (dwEndPos % FRAMESIZE);
		}

		// go to the place
		//
		dwCurPos = SetFilePointer(hChunksFile, dwSeekPos, NULL, SEEK_SET);

		// check for a valid position
		//
		if (dwCurPos == dwSeekPos)
		{
			// perform the read file
			//
			DWORD dwBytesRead = 0;

			ZeroMemory(m_FileBuff, FRAMESIZE);
			ReadFile(hChunksFile, m_FileBuff, FRAMESIZE, &dwBytesRead, NULL);

			b_ret = (0 != dwBytesRead);
		}

		// will handle the -1 case for m_iSeekChunk
		//
		m_iSeekChunk = dwCurPos / FRAMESIZE;

		CloseHandle((HANDLE)hChunksFile);
	} 

	return b_ret;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::ShowFileChunk(void)
{
	if (true == ReadFileChunk())
	{
		// use the frame buffer for the socket to pass data through
		//
		memcpy(m_FrameBuff, m_FileBuff, FRAMESIZE);
		SetNewPlotData();

		// update the edit control
		//
		CString sTemp;

		__reentry = true;
		sTemp.Format(L"%d", m_iSeekChunk);
		m_EdSeekValue.SetWindowTextW(sTemp);
		__reentry = false;

		// ask for a redraw
		//
		m_DrawChanged = true;
		PostMessage(WM_PAINT);

		return true;
	}

	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

DWORD CFakeReaderDlg::GetSeekPosFromEdit(void)
{
	CString sValue;

	m_EdSeekValue.GetWindowTextW(sValue);

	// if there is no text then put a zero
	//
	if (true == sValue.IsEmpty())
	{
		__reentry = true;
		sValue.Format(L"%d", m_iSeekChunk);
		m_EdSeekValue.SetWindowTextW(sValue);
		__reentry = false;
	}

	return _wtol(sValue);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnEnUpdateEdSeekValue()
{
	CString szText;

	if (false == __reentry && false == m_bTimerActive)
	{
		DWORD	dwValue = GetSeekPosFromEdit();

		if (dwValue != m_iSeekChunk)
		{
			m_iSeekChunk = dwValue;
			ShowFileChunk();
		}

		UpdateTimesAt(m_iSeekChunk);
	}

	// when timer is active
	//
	if (true == m_bTimerActive)
	{
		UpdateTimesAt(m_iLoop);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

LPCWSTR STRING_FORMAT_TIME_MILLI	= L"[0.%03d]";
LPCWSTR STRING_FORMAT_TIME_SECONDS	= L"[%2d.%03d]";
LPCWSTR STRING_FORMAT_TIME_MINUTES	= L"[%2d:%02d.%03d]";

void CFakeReaderDlg::UpdateTimesAt(size_t inCounter)
{
	CString		szText;
	size_t		uTimeValue = inCounter * m_uTimerSpeed;

	if (1000 >= uTimeValue)
	{
		szText.Format(STRING_FORMAT_TIME_MILLI, uTimeValue);
	}
	else
	{
		size_t uRemMilli = uTimeValue % 1000;

		uTimeValue /= 1000;
		if (60 > uTimeValue) 
		{
			szText.Format(STRING_FORMAT_TIME_SECONDS, uTimeValue, uRemMilli);
		}
		else
		{
			size_t uRemSecs = uTimeValue % 60;
				
			uTimeValue /= 60;
			szText.Format(STRING_FORMAT_TIME_MINUTES, uTimeValue, uRemSecs, uRemMilli);
		}
	}

	m_txTimesAt.SetWindowTextW(szText);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtnSeekFirst()
{
	if (false == m_bTimerActive)
	{
		m_iSeekChunk = 0;
		ShowFileChunk();
		UpdateTimesAt(m_iSeekChunk);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtnSeekLast()
{
	if (false == m_bTimerActive)
	{
		// will get the correct seek pos...
		//
		m_iSeekChunk = -1;
		ShowFileChunk();
		UpdateTimesAt(m_iSeekChunk);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtnSeekPrev()
{
	if (false == m_bTimerActive)
	{
		m_iSeekChunk--;
		if (0 > m_iSeekChunk)
		{
			m_iSeekChunk = 0;
		}
		ShowFileChunk();
		UpdateTimesAt(m_iSeekChunk);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnBnClickedBtnSeekNext()
{
	if (false == m_bTimerActive)
	{
		m_iSeekChunk++;
		ShowFileChunk();
		UpdateTimesAt(m_iSeekChunk);
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnStnDblclickWinplot()
{
	ShowGraphicsCtrls();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#include "DlgPlotSettings.h"

static CDlgPlotSettings * m_DlgPlotSettings = NULL;

void CFakeReaderDlg::ShowGraphicsCtrls(void)
{
	if (NULL != m_DlgPlotSettings)
	{
		AlignGraphicsCtrls();
		m_DlgPlotSettings->ShowWindow(SW_SHOWNORMAL);
		return;
	}

	m_DlgPlotSettings = new CDlgPlotSettings(this);

	m_DlgPlotSettings->Create(IDD_DLG_PLOTSETTINGS, this);
	m_DlgPlotSettings->ShowWindow(SW_SHOWNORMAL);
	AlignGraphicsCtrls();

	// set the graphics control
	//
	m_DlgPlotSettings->AttachToPlotter(& m_Plotter);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#include "DlgBioTelSettings.h"

static CDlgBioTelSettings * m_DlgBioTelSettings = NULL;

void CFakeReaderDlg::ShowBioTelCtrls(void)
{
	if (NULL != m_DlgBioTelSettings)
	{
		AlignBioTelCtrls();
		m_DlgBioTelSettings->ShowWindow(SW_SHOWNORMAL);
		return;
	}

	m_DlgBioTelSettings = new CDlgBioTelSettings(this);

	m_DlgBioTelSettings->Create(IDD_DLG_BIOTEL, this);
	m_DlgBioTelSettings->ShowWindow(SW_SHOWNORMAL);
	AlignBioTelCtrls();

	// set the graphics control
	//
	m_DlgBioTelSettings->AttachMainDialog(this);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::HideGraphicsCtrls()
{
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::OnWindowPosChanged(WINDOWPOS* lpwndpos)
{
	CDialogEx::OnWindowPosChanged(lpwndpos);

	AlignGraphicsCtrls();
	AlignBioTelCtrls();
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::AlignGraphicsCtrls(void)
{
	if (NULL == m_DlgPlotSettings)
	{
		return;
	}

	CRect rcThisDlg, rcNewDlg;
	long  iTemp;

	this->GetWindowRect(rcThisDlg);
	m_DlgPlotSettings->GetWindowRect(rcNewDlg);

	// check if we have space enough to draw the window
	//
	iTemp = rcThisDlg.left - 5 - rcNewDlg.Width();

	if (0 > iTemp)
	{
		rcThisDlg.left = rcThisDlg.right + 5;
	}
	else
	{
		rcThisDlg.left = iTemp;
	}

	m_DlgPlotSettings->SetWindowPos(this, rcThisDlg.left, rcThisDlg.top, rcNewDlg.Width(), rcNewDlg.Height(), 0);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::AlignBioTelCtrls(void)
{
	if (NULL == m_DlgBioTelSettings)
	{
		return;
	}

	CRect rcThisDlg, rcNewDlg;
	long  iTemp;

	this->GetWindowRect(rcThisDlg);
	m_DlgBioTelSettings->GetWindowRect(rcNewDlg);

	// check if we have space enough to draw the window
	//
	iTemp = rcThisDlg.bottom + 5;

	//if (0 > iTemp)
	//{
	//	rcThisDlg.left = rcThisDlg.right + 5;
	//}
	//else
	{
		rcThisDlg.top = iTemp;
	}

	m_DlgBioTelSettings->SetWindowPos(this, rcThisDlg.left, rcThisDlg.top, rcNewDlg.Width(), rcNewDlg.Height(), 0);
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::LoadBioTelemetry(void)
{
	CString			sFilename = L"BioTel.dll";
	CStdioFile		hfBioTel;
	bool			bReturn = false;

	// reset here
	//
	UnloadBioTelemetry();

	if (TRUE == hfBioTel.Open(sFilename, CFile::modeRead))
	{
		hfBioTel.Close();

		if (NULL != (m_hBioTelLib = LoadLibrary(sFilename)))
		{
			// scan for known functions
			//
			extfx_Runner_Params = (pfnRunnerParams)GetProcAddress(m_hBioTelLib, "Set_ECG_Parameters");
			extfx_InitRunner	= (pfnRunnerInit)GetProcAddress(m_hBioTelLib,	"Init_ECG_Runner");
			extfx_Run_Runner	= (pfnRunnerRun)GetProcAddress(m_hBioTelLib,	"Run_ECG_Task_Ratio");

			// check all function pointers are valid
			//
			bReturn = (NULL != extfx_InitRunner && NULL != extfx_Run_Runner && NULL != extfx_Runner_Params);

			if (false == bReturn)
			{
				UnloadBioTelemetry();

				AppendLogText(L"[BioTel.dll] Library loaded but missing functions.");
			}
			else
			{
				AppendLogText(L"[BioTel.dll] Library loaded.");
			}
		}
		else
		{
			AppendLogText(L"[BioTel.dll] Library found but load failed for some reason.");
		}
	}
	else
	{
		AppendLogText(L"[BioTel.dll] File not found.");
	}

	return bReturn;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::UnloadBioTelemetry(void)
{
	if (NULL != m_hBioTelLib)
	{
		FreeLibrary(m_hBioTelLib);
	}

	m_hBioTelLib		= NULL;
	extfx_Runner_Params = NULL;
	extfx_InitRunner	= NULL;
	extfx_Run_Runner	= NULL;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::ConfigBioTelemetry(void)
{
	CString sMessage;

	if (false == m_sBioTelParams_0.IsEmpty())
	{
		float fParams_0[9];

		VectorFromText(m_sBioTelParams_0, fParams_0, 9);			// be careful to respect the number of parameters

		((*extfx_Runner_Params) (0, fParams_0));

		sMessage.Format(L"[ ZCC] = [%s]", m_sBioTelParams_0);
		AppendLogText(sMessage);
	}

	if (false == m_sBioTelParams_1.IsEmpty())
	{
		float fParams_1[1];

		VectorFromText(m_sBioTelParams_1, fParams_1, 1);			// be careful

		((*extfx_Runner_Params) (1, fParams_1));

		sMessage.Format(L"[TIME] = [%s]", m_sBioTelParams_1);
		AppendLogText(sMessage);
	}

	if (false == m_sBioTelParams_2.IsEmpty())
	{
		float fParams_2[7];

		VectorFromText(m_sBioTelParams_2, fParams_2, 7);			// be careful

		((*extfx_Runner_Params) (2, fParams_2));

		sMessage.Format(L"[FREQ] = [%s]", m_sBioTelParams_2);
		AppendLogText(sMessage);
	}

	try
	{
		if (true == ((*extfx_InitRunner) ()))
		{
			AppendLogText(L"[BioTel.dll] Initialization completed (init training).");
		}
		else
		{
			AppendLogText(L"[BioTel.dll] Initialization failed.");
		}
	}
	catch (std::invalid_argument)
	{
		AppendLogText(L"Parameters have one or more invalid value.");
	}
	catch (std::logic_error)
	{
		AppendLogText(L"[BioTel.dll] Initialization failed.");
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// read a string in this example format:
// 10.0f, 25.0f, 50.0f, 4.0f, 0.001f, 0.001f, 0.1f, 0.1f, 2.0f
//
bool CFakeReaderDlg::VectorFromText(const CString &inText, float outVector[], int inMaxBound)
{
	int iStart = 0;
	CString sValue = inText.Tokenize(L",", iStart);

	// loop until elements are found and/or elements are less that bound
	//
	for (int sz = 0; sz < inMaxBound && 0 < iStart; sz++)
	{
		// do the conversion from text to binary
		//
		outVector[sz] = (float)_ttof(sValue);

		// get the next value in string
		//
		sValue = inText.Tokenize(L",", iStart);
	}

	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::SetBioTelParam_0(const CString & inString)
{
	m_sBioTelParams_0 = inString;

	if (NULL != m_hBioTelLib)
	{
		if (false == m_sBioTelParams_0.IsEmpty())
		{
			ConfigBioTelemetry();
		}
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::SetBioTelParam_1(const CString & inString)
{
	m_sBioTelParams_1 = inString;

	if (NULL != m_hBioTelLib)
	{
		if (false == m_sBioTelParams_1.IsEmpty())
		{
			ConfigBioTelemetry();
		}
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

void CFakeReaderDlg::SetBioTelParam_2(const CString & inString)
{
	m_sBioTelParams_2 = inString;

	if (NULL != m_hBioTelLib)
	{
		if (false == m_sBioTelParams_2.IsEmpty())
		{
			ConfigBioTelemetry();
		}
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ini file settings
//
LPCWSTR s_Line_Header			= L"-- FakeReader initialization";
LPCWSTR s_Line_Footer			= L"\n-- End of file\n";
LPCWSTR	s_Line_Comment			= L"--";
LPCWSTR	s_Line_LocalPort		= L"LOCALPORT=";
LPCWSTR	s_Line_TimerSpeed		= L"TIMERSPEED=";

LPCWSTR	s_Line_DrawMethod		= L"DRAWMETHOD=";
LPCWSTR	s_Line_Channel			= L"SELECTCHANNEL=";
LPCWSTR	s_Line_Recent			= L"RECENTFILE=";
LPCWSTR	s_Line_Analisys			= L"ANALISYS=";

LPCWSTR	s_Line_LineRef_X		= L"REFLINEX=";
LPCWSTR	s_Line_LineRef_Y		= L"REFLINEY=";
LPCWSTR	s_Line_Scale_X			= L"SCALEX=";
LPCWSTR	s_Line_Scale_Y			= L"SCALEY=";
LPCWSTR	s_Line_ViewPort_X		= L"VIEWPORTX=";
LPCWSTR	s_Line_ViewPort_Y		= L"VIEWPORTY=";

LPCWSTR	s_Line_BioPar0			= L"BIOTEL_PARAM_ZCC=";
LPCWSTR	s_Line_BioPar1			= L"BIOTEL_PARAM_TIME=";
LPCWSTR	s_Line_BioPar2			= L"BIOTEL_PARAM_FREQ=";

LPCWSTR	s_Line_WindowPos_XY		= L"WINDOWPOSXY=";

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

bool CFakeReaderDlg::LoadSettings(void)
{
	CStdioFile  hfSettings;
	if (TRUE == hfSettings.Open(DEF_CONFIG_FNAME, CStdioFile::modeRead | CStdioFile::typeText))
	{
		CString		sLineFromFile;
		CString		sLineRead;
		CString		sValue;
		int			iValue;
		size_t		uValue;
		float		fValue;

		while (TRUE == hfSettings.ReadString(sLineFromFile))
		{
			// clean the string and align it to the left
			//
			sLineRead = sLineFromFile.Trim(L"\t\r\n ");

			// skip empty lines and comments
			//
			if (0 == sLineRead.GetLength() || 0 == sLineRead.Find(s_Line_Comment))
			{
				continue;
			}
			// ip local port
			//
			if (0 == sLineRead.Find(s_Line_LocalPort))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_LocalPort));
				m_uLocalPort = (USHORT)_wtol(sValue);

				if (0 == m_uLocalPort)
				{
					m_uLocalPort = BIND_TO_PORT;
				}
				continue;
			}
			// timer speed
			//
			if (0 == sLineRead.Find(s_Line_TimerSpeed))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_TimerSpeed));
				m_uTimerSpeed = (USHORT)_wtol(sValue);

				if (TIMER_INTERVAL_FAST > m_uTimerSpeed)
				{
					m_uTimerSpeed = TIMER_INTERVAL_BIOBOX;
				}
				continue;
			}
			// current file selected
			//
			if (0 == sLineRead.Find(s_Line_Recent))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_Recent));
				uValue = (size_t)_wtol(sValue);;

				if (uValue < (size_t)m_LsRecentFiles.GetCount())
				{					
					m_LsRecentFiles.SetCurSel((int)uValue);
					OnLbnSelchangeLsfilename();
				}
				continue;
			}
			// channel selection
			//
			if (0 == sLineRead.Find(s_Line_Channel))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_Channel));
				m_uDrawChannel = (USHORT)_wtol(sValue);

				if (5 <= m_uDrawChannel)
				{
					m_uDrawChannel = 0;
				}
				continue;
			}
			// drawing method
			//
			if (0 == sLineRead.Find(s_Line_DrawMethod))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_DrawMethod));
				m_eDrawType = (eDrawFunction)_wtol(sValue);

				if (eDraw_Fx_Max <= m_eDrawType)
				{
					m_eDrawType = eDraw_Fx_Segments;
				}
				continue;
			}
			// reference line X
			//
			if (0 == sLineRead.Find(s_Line_LineRef_X))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_LineRef_X));
				iValue = (int)_wtol(sValue);

				if (iValue >= 0)
				{
					m_Plotter.OverrideRefLine(iValue, m_Plotter.LineRefY());
				}
				continue;
			}
			// reference line Y
			//
			if (0 == sLineRead.Find(s_Line_LineRef_Y))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_LineRef_Y));
				iValue = (int)_wtol(sValue);

				if (iValue >= 0)
				{
					m_Plotter.OverrideRefLine(m_Plotter.LineRefX(), iValue);
				}
				continue;
			}
			// drawing scale X
			//
			if (0 == sLineRead.Find(s_Line_Scale_X))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_Scale_X));
				fValue = (float)_wtof(sValue);

				if (fValue != 0.0f)
				{
					m_Plotter.OverrideScale(fValue, m_Plotter.ScaleY());
				}
				continue;
			}
			// drawing scale Y
			//
			if (0 == sLineRead.Find(s_Line_Scale_Y))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_Scale_Y));
				fValue = (float)_wtof(sValue);

				if (fValue != 0.0f)
				{
					m_Plotter.OverrideScale(m_Plotter.ScaleX(), fValue);
				}
				continue;
			}
			// viewport offset X
			//
			if (0 == sLineRead.Find(s_Line_ViewPort_X))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_ViewPort_X));
				iValue = (int)_wtol(sValue);

				if (iValue >= 0)
				{
					m_Plotter.OverrideViewPort(iValue, m_Plotter.ViewPortY());
				}
				continue;
			}
			// viewport offset Y
			//
			if (0 == sLineRead.Find(s_Line_ViewPort_Y))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_ViewPort_Y));
				iValue = (int)_wtol(sValue);

				if (iValue >= 0)
				{
					m_Plotter.OverrideViewPort(m_Plotter.ViewPortX(), iValue);
				}
				continue;
			}
			// statistical analisys enable
			//
			if (0 == sLineRead.Find(s_Line_Analisys))
			{
				m_Plotter.EnableAnalisys((0 < sLineRead.Find(L"TRUE")));
				continue;
			}
			// biotel param 1
			//
			if (0 == sLineRead.Find(s_Line_BioPar0))
			{
				m_sBioTelParams_0 = sLineRead.Mid((int)wcslen(s_Line_BioPar0));
				continue;
			}
			// biotel param 2
			//
			if (0 == sLineRead.Find(s_Line_BioPar1))
			{
				m_sBioTelParams_1 = sLineRead.Mid((int)wcslen(s_Line_BioPar1));
				continue;
			}
			// biotel param 3
			//
			if (0 == sLineRead.Find(s_Line_BioPar2))
			{
				m_sBioTelParams_2 = sLineRead.Mid((int)wcslen(s_Line_BioPar2));
				continue;
			}
			// Window pos XY
			//
			if (0 == sLineRead.Find(s_Line_WindowPos_XY))
			{
				sValue = sLineRead.Mid((int)wcslen(s_Line_WindowPos_XY));
				iValue = sValue.Find(L",");

				if (0 < iValue)
				{
					CString		sTemp;
					long		lPosX, lPosY;
					long		lSizeX, lSizeY;

					sTemp = sValue.Mid(0, iValue);
					lPosX = (int)_wtol(sTemp);

					sTemp = sValue.Mid(iValue + 1);
					lPosY = (int)_wtol(sTemp);

					CRect	rc;
					GetWindowRect(&rc);

					lSizeX = rc.Width();
					lSizeY = rc.Height();

					MoveWindow(lPosX, lPosY, lSizeX, lSizeY);
				}
				continue;
			}
		}

		hfSettings.Close();
		return true;
	}

	return false;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

LPCWSTR SZFORMAT_TEXT_INTEGER	= L"%s%d\n";
LPCWSTR SZFORMAT_TEXT_FLOAT		= L"%s%.4f\n";
LPCWSTR SZFORMAT_TEXT_TEXT		= L"%s%s\n";
LPCWSTR SZFORMAT_TEXT_WINPOS	= L"%s%d,%d\n";

bool CFakeReaderDlg::SaveSettings(void)
{
	CString		sLineValue;
	CStdioFile  hfSettings;

	if (FALSE == hfSettings.Open(DEF_CONFIG_FNAME, CStdioFile::modeCreate | CStdioFile::modeWrite | CStdioFile::typeText))
	{
		return false;
	}

	// header
	//
	sLineValue.Format(L"%s\n%s\n%s\n", s_Line_Comment, s_Line_Header, s_Line_Comment);
	hfSettings.WriteString(sLineValue);

	// Window position
	//
	{
		CRect	rc;
		GetWindowRect(&rc);

		sLineValue.Format(SZFORMAT_TEXT_WINPOS, s_Line_WindowPos_XY, rc.left, rc.top);
		hfSettings.WriteString(sLineValue);
	}

	// local port
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_LocalPort, m_uLocalPort);
	hfSettings.WriteString(sLineValue);

	// timer speed
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_TimerSpeed, m_uTimerSpeed);
	hfSettings.WriteString(sLineValue);

	// recent file selected in list
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_Recent, m_LsRecentFiles.GetCurSel());
	hfSettings.WriteString(sLineValue);		

	// draw method
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_DrawMethod, m_eDrawType);
	hfSettings.WriteString(sLineValue);

	// draw channel
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_Channel, m_uDrawChannel);
	hfSettings.WriteString(sLineValue);

	// analisys
	//
	sLineValue.Format(SZFORMAT_TEXT_TEXT, s_Line_Analisys, m_Plotter.IsAnalisysEnabled()? L"TRUE": L"");
	hfSettings.WriteString(sLineValue);

	// plotter's reference line
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_LineRef_X, m_Plotter.LineRefX());
	hfSettings.WriteString(sLineValue);

	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_LineRef_Y, m_Plotter.LineRefY());
	hfSettings.WriteString(sLineValue);

	// plotter's scale
	//
	sLineValue.Format(SZFORMAT_TEXT_FLOAT, s_Line_Scale_X, m_Plotter.ScaleX());
	hfSettings.WriteString(sLineValue);

	sLineValue.Format(SZFORMAT_TEXT_FLOAT, s_Line_Scale_Y, m_Plotter.ScaleY());
	hfSettings.WriteString(sLineValue);

	// plotter's viewport offset
	//
	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_ViewPort_X, m_Plotter.ViewPortX());
	hfSettings.WriteString(sLineValue);

	sLineValue.Format(SZFORMAT_TEXT_INTEGER, s_Line_ViewPort_Y, m_Plotter.ViewPortY());
	hfSettings.WriteString(sLineValue);

	// BioTel's string parameters
	//
	sLineValue.Format(SZFORMAT_TEXT_TEXT, s_Line_BioPar0, m_sBioTelParams_0);
	hfSettings.WriteString(sLineValue);

	sLineValue.Format(SZFORMAT_TEXT_TEXT, s_Line_BioPar1, m_sBioTelParams_1);
	hfSettings.WriteString(sLineValue);

	sLineValue.Format(SZFORMAT_TEXT_TEXT, s_Line_BioPar2, m_sBioTelParams_2);
	hfSettings.WriteString(sLineValue);

	// here we are done with this
	//
	hfSettings.WriteString(s_Line_Footer);
	hfSettings.Close();
	return true;
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------


