//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#include "stdafx.h"
#include "SincTable.h"

#define _USE_MATH_DEFINES
#include "math.h"

#define PIGRECO                      (float)M_PI
#define DUE_PIGRECO                  (float)( 2.00000f * PIGRECO )

//-----------------------------------------------------------------------------
//	class:		CSincTable
//-----------------------------------------------------------------------------

CSincTable::CSincTable(void)
{
	m_SincTable = NULL;
	Release();
}
 
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

CSincTable::~CSincTable(void)
{
	Release();
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
void CSincTable::Release(void)
{
	if (NULL != m_SincTable)
	{
		delete[] m_SincTable;
		m_SincTable = NULL;
	}

	m_SincType		= eSinc_Fx_Invalid;
	m_MaxElements	= 0;
	m_TimeIncrement	= 0;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
float & CSincTable::operator [](UINT inIndex)
{
	if (inIndex < m_MaxElements)
	{
		return m_SincTable[inIndex];
	}

	TRACE(L"CSincTable::operator [][%d] <-- Indexing Error", inIndex);

	//	this is safe, max can be 0 but at least 1 is created
	//
	return m_SincTable[0];
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
float CSincTable::__SincFunction(float x) const
{
	float    dReturn;

	if (0.1f > fabs(x))
	{
		return 1.000f;
	}

	dReturn = (float)(sin(x) / (double)x);

	return dReturn;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
float CSincTable::__NormSincFunction(float x) const
{
	float    index = (x / m_TimeIncrement);
	float    tau = (float)(m_MaxElements - 1);
	float    dReturn;


	if (0.1f > fabs(x))
	{
		return 0.000f;
	}

	dReturn = __SincFunction(PIGRECO * index / tau) / (x);

	return dReturn;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//	implements the Blackman window

#pragma warning (disable: 4244 )
float	CSincTable::__WindowedSincFunction(float x) const
{
	float    index = (x / m_TimeIncrement);
	float    tau = (float)(m_MaxElements - 1);
	float    alpha = 0.500f;
	float    beta = 0.420f;
	float    gamma = 0.080f;
	float    dReturn;


	if (0.0001f > fabs(x))
	{
		return 0.0f;
	}

	dReturn = beta + (1.0F - alpha) * cos((PIGRECO * index) / tau) + gamma * cos((DUE_PIGRECO * index) / tau);

	return dReturn / x;
}
#pragma warning (default: 4244 )

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//
bool CSincTable::Setup(eSincFxTypes inType, size_t inMaxElements, size_t inTimeInterval)
{
	float    dResult;
	float    iTimesAt = 0.0f;
	UINT     k = 0;


	//	check if data changed
	//
	if ((inType == m_SincType) && (inMaxElements == m_MaxElements) && (inTimeInterval == m_TimeIncrement))
	{
		return true;
	}

	//	check if data valid
	//
	if ((eSinc_Fx_Invalid == inType) || (0 == inMaxElements) || (0 == inTimeInterval))
	{
		return false;
	}

	TRACE(L"[BioSinc] Setup sinc table of [%d] elements timed at [%d]\n", inMaxElements, inTimeInterval);

	//	destroy the old table
	//
	Release();

	//	create the table
	//
	m_SincTable = new float[inMaxElements];

	//	setup properties
	//
	m_SincType     = inType;
	m_MaxElements  = inMaxElements;
	m_TimeIncrement = inTimeInterval;

	//	assign values to table
	//
	while (k < inMaxElements)
	{
		switch (inType)
		{
		default:
		case eSinc_Fx_Original:

			dResult = __SincFunction(iTimesAt);
//			TRACE(L"[BioSinc] SINC [%.0f,%.5f]\n", iTimesAt, dResult);
			break;

		case eSinc_Fx_Normalized:

			dResult = __NormSincFunction(iTimesAt);
//			TRACE(L"[BioSinc] NORMAL [%.0f,%.5f]\n", iTimesAt, dResult);
			break;

		case eSinc_Fx_Windowed:

			dResult = __WindowedSincFunction(iTimesAt);
//			TRACE(L"[BioSinc] BLACKMAN [%.0f,%.5f]\n", iTimesAt, dResult);
			break;
		}

		m_SincTable[k++] = dResult;
		iTimesAt += inTimeInterval;
	}

	return true;
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
