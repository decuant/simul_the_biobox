// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

#pragma once
#include "Background.h"
#include "SincTable.h"
#include "DataAnalysis.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

#define SIZE_GRID_COLUMNS		10
#define SIZE_GRID_ROWS			6500

//	PARAMETERS FOR THE DELETEDC
//
//#define DELETE_DC_TOP       0x00001
//#define DELETE_DC_LEFT      0x00010
//#define DELETE_DC_RIGHT     0x00100
//#define DELETE_DC_LEGENDA   0x01000
#define DELETE_DC_DRAW      0x10000
#define DELETE_DC_ALL       0x11111

#define DRAW_MAX_SAMPLES	80

//-----------------------------------------------------------------------------
//	enum:		eDrawFunction
//-----------------------------------------------------------------------------

enum eDrawFunction
{
	eDraw_Fx_Disabled  = 0,
	eDraw_Fx_DotsOnly,
	eDraw_Fx_Segments,              //	this is the default
//	eDraw_Fx_Bezier,
	eDraw_Fx_Sinc,
	eDraw_Fx_Hitmap,				// no background used
	eDraw_Fx_Analisys,				// "    "   "   "   "

	//eDraw_Fx_NormalizedSinc,
	//eDraw_Fx_WindowedSinc,
	//eDraw_Fx_CubicI,

	eDraw_Fx_Max
};

//-----------------------------------------------------------------------------
//	class:		CPlotView
//-----------------------------------------------------------------------------

class CPlotView
{
private:

	CBackground		* m_Background;							// background grid

	CDC				* m_DcDraw;								// dc for the drawing
	CRect			m_RectDraw;								// size of our drawing
	int				m_ColWidth;								// size of column
	int				m_RowHeight;							// size of row

	eDrawFunction	m_eDrawFunction;						// method for drawing
	CBrush			* m_BrushDot;
	CPen			* m_PenChannel1;
	CPen			* m_PenChannel2;
	CPen			* m_PenSinc;
	CBrush			* m_BrushHit;
	CPen			* m_PenHit;
	CBrush			* m_BrushBars;

	CPoint			m_ptRefPos;								// reference point
	CPoint			m_ptViewport;
	float			m_fScaleX;								// zoom for the X axis
	float			m_fScaleY;								// zoom for the Y axis

	CSincTable		m_SincTable;							// precalc table

	size_t			m_PointsNumber;							// size of internal vector
	CPoint			* m_vPoints;							// data points

	CDataAnalysis	m_Analisys;								// performs analisys
	float			m_fYScale;								// adapting the scaling
private:

	void    Reset(void);										// cleanup at start time
	void    DeleteAllPens(void);								// delete pens when finished
	void    DeleteDCs(DWORD inFlag = DELETE_DC_ALL);			// delete the specified dc
	void	ReleaseGDIObj(CGdiObject * inWhich);

	void	RecalcScale(void);									// update the scale values
	void	MapToOriginEx(CPoint & ioPoint);					// map a value to the client rect
	void	MapToOriginSinc(CPoint & ioPoint);					// tbd hack to offset zero to middle of rect's height
	void	InternalDrawDrawing(CDC * inDc);					// switch the draw

	void	InternalDrawDotsOnly(CDC * inDc);					// draw using rectangles
	void	InternalDrawOneLine(CDC * inDc);					// drwaw using a line of connected segments
	void	InternalDrawOneLineSincFx(CDC * inDc);				// perform the sinc function on data

	// the following functions don't use the background
	//
	void	InternalDrawHitMap(CDC * inDc);						// show all points hit map
	void	InternalDrawAnalisys(CDC * inDc);					// data analisys

public:

	CPlotView();
	~CPlotView();

	void SetWindowRect(const CRect  & inRect);					// set the surface area
	void Draw(CDC * inDc);										// main call for drawing

	// set/get the modified status
	//
	inline bool IsModified(void) const { return (NULL == m_DcDraw); }
	inline void Invalidate(void) { DeleteDCs(DELETE_DC_ALL); }

	void ForceRefresh(void);

	// allocate a vector big enough to store data in
	//
	void InitializeMemory(size_t inNumPoints, size_t inTimeSpan);
	void UpdateData(const CPoint inPoints[], size_t inNumPoints);

	// decide which drawing function to use
	//
	void SetDrawingFunction(eDrawFunction inValue);
	inline eDrawFunction DrawFunction(void) const { return m_eDrawFunction; }

	// scale for the drawing
	//
	inline float ScaleX(void) const { return m_fScaleX; }
	inline float ScaleY(void) const { return m_fScaleY; }
	void OverrideScale(const float & inScaleX, const float & inScaleY);

	// reference lines
	//
	inline int LineRefX(void) const { return m_ptRefPos.x; }
	inline int LineRefY(void) const { return m_ptRefPos.y; }
	void OverrideRefLine(size_t inOffsetX, size_t inOffsetY);

	// viewport offset
	//
	inline int ViewPortX(void) const { return m_ptViewport.x; }
	inline int ViewPortY(void) const { return m_ptViewport.y; }
	void OverrideViewPort(size_t inOffsetX, size_t inOffsetY);

	// colors of the plot
	//
	void SetClr_Plot_Dots(const COLORREF & inColor);
	void SetClr_Plot_Segments(const COLORREF & inColor);
	void SetClr_Plot_SincFx(const COLORREF & inColor);
	void SetClr_Plot_Hits(const COLORREF & inBrushClr, const COLORREF & inPenClr);
	void SetClr_Plot_Bars(const COLORREF & inBrushClr);

	// colors of the background
	//
	void SetClr_Back_Background(const COLORREF & inColor);
	void SetClr_Back_GridLines(const COLORREF & inColor);
	void SetClr_Back_Reference(const COLORREF & inColor);
	void SetClr_Back_Border(const COLORREF & inColor);

	// analisys
	//
	void SetStartTime(void);
	void SetStopTime(void);
	void EnableAnalisys(bool inFlag);
	bool IsAnalisysEnabled(void) const;
};


/*
void	InternalDrawBezier(CDC * inDc);												// draw using Bezier interpolation

void	__Bezier4(const CPoint inPoints[], CPoint & ouPoint, double mu) const;		// accept a vector of 4 points
*/

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
